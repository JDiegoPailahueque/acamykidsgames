# AcamyKidsGames

## Description
AcamyKidsGames is an educational and entertaining mobile application designed for children. It offers a variety of games and activities that help children learn while having fun. The application is developed using Flutter, a cross-platform mobile framework.

## Requirements
To develop and run this application, you need the following requirements:

- **Flutter**: Version 3.19.6
- **Android SDK**: Installed and configured
- **Xcode**: For developing and testing on iOS devices

## Installation
Follow these steps to set up and run the project in your local development environment.

### Step 1: Clone the repository
Clone this repository to your local machine using the following command:

```bash
git clone https://gitlab.com/JDiegoPailahueque/acamykidsgames.git
cd acamykidsgames
```

### Step 2: Create the Flutter project
Generate Android and iOS directories:

```bash
flutter create . --platforms=android,ios --org=com.nuevagestion
```

### Step 3: Configure the project for Android
Edit the `android/app/build.gradle` file and add the following line within `defaultConfig`:

```gradle
defaultConfig {
    ...
    multiDexEnabled true
}
```

### Step 4: Install dependencies
Run the following command to install the project dependencies:

```bash
flutter pub get
```

### Step 5: Run the application
Finally, run the application on your emulator or physical device:

```bash
flutter run
```

