import 'dart:io';

import 'package:acamykidsgames/src/models/games_model.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/acamykids/default_image.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class GameItem extends StatefulWidget {
  final double? height;
  final double? width;
  final Function onPress;
  final Game game;

  GameItem({
    Key? key,
    this.height,
    this.width,
    required this.onPress,
    required this.game
  }) : super(key: key);

  @override
  State<GameItem> createState() => _GameItemState();
}

class _GameItemState extends State<GameItem> {
  
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive(context);
    double _height = widget.height ?? responsive.ip(13);
    double _width = widget.width ?? responsive.ip(24);

    return Align(
      child: GestureDetector(
        onTap: () => widget.onPress(),
        child: Container(
          height: _height,
          width: _width,
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(responsive.ip(0.8))
          ),
          child: Stack(
            children: [
              Positioned.fill(
                child: Container(
                  color: Colors.white,
                  child: Builder(
                    builder: (context) {
                      if (widget.game.image?.urlDld != null)
                        return Image.file(
                          File('${widget.game.image!.urlDld}'),
                          fit: BoxFit.cover,
                          height: double.maxFinite,
                          width: double.maxFinite,
                          errorBuilder: (context, error, stackTrace) => Container(
                            margin: EdgeInsets.only(bottom: responsive.ip(2.5)),
                            child: DefaultImage(
                              fontSize: 2.5,
                              strokeWidth: 3.5,
                            ),
                          ),
                        );

                      return (widget.game.image?.url != null && widget.game.image?.url != "null")
                      ? CachedNetworkImage(
                        height: double.maxFinite,
                        width: double.maxFinite,
                        fit: BoxFit.cover,
                        imageUrl: widget.game.image!.url,
                        placeholder: (context, url) => Container(
                          margin: EdgeInsets.only(bottom: responsive.ip(2.5)),
                          child: DefaultImage(
                            fontSize: 2.5,
                            strokeWidth: 3.5,
                          ),
                        ),
                        errorWidget: (context, url, error) => Container(
                          margin: EdgeInsets.only(bottom: responsive.ip(2.5)),
                          child: DefaultImage(
                            fontSize: 2.5,
                            strokeWidth: 3.5,
                          ),
                        ),
                      ) : Container(
                        margin: EdgeInsets.only(bottom: responsive.ip(2.5)),
                        child: DefaultImage(
                          fontSize: 2.5,
                          strokeWidth: 3.5,
                        ),
                      );
                    },
                  )
                ),
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: responsive.ip(1),
                    vertical: responsive.ip(0.3)
                  ),
                  color: Colors.black.withOpacity(0.56),
                  height: responsive.ip(3.3),
                  width: _width,
                  child: Container(
                    width: double.maxFinite,
                    child: Text(
                      '${widget.game.name}',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: responsive.ip(1.4)
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}