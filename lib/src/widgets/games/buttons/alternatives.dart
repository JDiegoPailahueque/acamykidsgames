import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';

class Alternatives extends StatelessWidget {
  final List<String> alternatives;
  final Function(String) sendAnswer;

  Alternatives({
    required this.alternatives,
    required this.sendAnswer,
  });
  @override
  Widget build(BuildContext context) {
    Responsive resp = Responsive(context);
    return Container(
      alignment: Alignment.bottomCenter,
      child: Row(
        children: List.generate(
          alternatives.length,
          (index) => Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: resp.ip(1.2)),
              margin: EdgeInsets.only(bottom: resp.ip(1)),
              height: resp.ip(6),
              child: MaterialButton(
                disabledColor: Colors.grey,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                color: Colors.white,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: GradientText(
                    text: alternatives[index],
                    fontSize: 2.15,
                  ),
                ),
                onPressed: () => sendAnswer(alternatives[index]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
