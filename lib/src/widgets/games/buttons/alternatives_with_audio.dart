import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';

class AlternativesWithAudio extends StatelessWidget {
  final BaseGameModel? baseGameModel;
  final List<String> alternatives;
  final Function(String) sendAnswer;
  
  AlternativesWithAudio({
    this.baseGameModel,
    required this.alternatives,
    required this.sendAnswer,
  });
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    return Container(
      alignment: Alignment.bottomCenter,
      child: Row(
        children: List.generate(
          alternatives.length,
          (index) => Expanded(
            child: Stack(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.only(
                    left: responsive.ip(3),
                    right: responsive.ip(1),
                  ),
                  margin: EdgeInsets.only(bottom: responsive.ip(1)),
                  height: responsive.ip(6),
                  child: MaterialButton(
                    disabledColor: Colors.grey,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    color: Colors.white,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: GradientText(
                        text: alternatives[index],
                        fontSize: 2.15,
                      ),
                    ),
                    onPressed: () => sendAnswer(alternatives[index]),
                  ),
                ),
                Container(
                  width: responsive.ip(6),
                  padding: EdgeInsets.only(
                    left: responsive.ip(1),
                  ),
                  child: MaterialButton(
                    child: Icon(FontAwesomeIcons.volumeHigh, color: Colors.white),
                    color: Colors.amber,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(responsive.ip(2)),
                    ),
                    padding: EdgeInsets.all(responsive.ip(1)),
                    onPressed: () {
                      baseGameModel?.loadAudio(alternatives[index], clearStr: true);
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
