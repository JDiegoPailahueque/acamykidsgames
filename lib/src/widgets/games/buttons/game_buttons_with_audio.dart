import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
import 'package:acamykidsgames/src/widgets/games/basic_button/game_button.dart';

class GameButtonsWithAudio extends StatelessWidget {
  final BaseGameModel? baseGameModel;
  final List<dynamic> options;
  final List<GameButtonStatus> states;
  final Function sendAnswer;
  final bool fitText;
  
  GameButtonsWithAudio({
    this.baseGameModel,
    required this.options,
    required this.states,
    required this.sendAnswer,
    this.fitText = false,
  });
  
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    return Row(
      children: List.generate(
        options.length,
        (i) => Expanded(
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                padding: EdgeInsets.only(
                  left: responsive.ip(3.5),
                  right: responsive.ip(1),
                ),
                margin: EdgeInsets.only(bottom: responsive.ip(1)),
                height: responsive.ip(6),
                child: GameButton(
                  fitText: fitText,
                  text: "${options[i]}",
                  status: states[i],
                  onPressed: () => sendAnswer(i),
                  padding: EdgeInsets.symmetric(
                    vertical: responsive.ip(1.5),
                  ),
                ),
              ),
              Container(
                width: responsive.ip(5.5),
                padding: EdgeInsets.only(
                  left: responsive.ip(0.7),
                ),
                child: MaterialButton(
                  child: Icon(FontAwesomeIcons.volumeUp, color: Colors.white),
                  color: Colors.amber,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(responsive.ip(2)),
                  ),
                  padding: EdgeInsets.all(responsive.ip(1)),
                  onPressed: () {
                    baseGameModel?.loadAudio("${options[i]}", clearStr: true);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
