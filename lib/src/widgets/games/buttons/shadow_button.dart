import 'dart:async';
import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/games/text/bordered_text.dart';

class ShadowButton extends StatefulWidget {
  final Function onPress;
  final Function? onLongPress;
  final String? text;
  final double fontSize;
  final double? width;
  final double? height;
  final Widget? child;
  final EdgeInsetsGeometry? margin;
  final List<Color> colors;
  final Color borderColor;
  
  ShadowButton({
    required this.onPress,
    this.onLongPress,
    this.text,
    this.fontSize = 3.2,
    this.width,
    this.height,
    this.margin,
    this.child,
    this.colors = const [
      // Color(0xffB39FEC),
      Color(0xf56893D3),
      Color(0xd94A1FC7),
    ],
    this.borderColor = Colors.white,
  });


  @override
  _ShadowButtonState createState() => _ShadowButtonState();
}

class _ShadowButtonState extends State<ShadowButton> {
  Offset offset = Offset(-4, 4);
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    return GestureDetector(
      onTap: () => widget.onPress(),
      onLongPress: (widget.onLongPress != null) ? () => widget.onLongPress!() : () => widget.onPress(),
      onLongPressEnd: (value) {
        setState(() => offset = Offset(-4, 4));
      },
      onTapDown: (value) {
        setState(() => offset = Offset(-0, 0));
      },
      onTapUp: (value) {
        Timer(Duration(milliseconds: 20), () {
          setState(() => offset = Offset(-4, 4));
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        margin: widget.margin ?? EdgeInsets.only(bottom: responsive.ip(1.0)),
        width: widget.width ?? responsive.wp(20),
        height: widget.height ?? responsive.ip(6),
        child: Center(
          child: widget.child ??
              BorderedText(
                text: widget.text ?? '',
                strokeWidth: 2,
                fontSize: widget.fontSize,
                colors: [Color(0xf56893D3), Color(0xd94A1FC7)],
              ),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          border: Border.all(width: 2, color: widget.borderColor),
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment(3, 0),
            colors: widget.colors,
          ),
          boxShadow: [
            BoxShadow(offset: offset, color: Color(0xffCDCDCD)),
          ],
        ),
      ),
    );
  }
}
