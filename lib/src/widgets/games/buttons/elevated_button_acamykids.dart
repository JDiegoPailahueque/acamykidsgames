import 'dart:async';
import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';

class ElevatedButtonAcamykids extends StatefulWidget {
  final Function onPress;
  final double? width;
  final double? height;
  final Widget? child;
  final EdgeInsetsGeometry? margin;
  final Color color;
  final Color shadowColor;
  
  ElevatedButtonAcamykids({
    required this.onPress,
    this.width,
    this.height,
    this.margin,
    this.child,
    this.color = Colors.white,
    this.shadowColor = Colors.black,
  });


  @override
  ElevatedButtonStateAcamykids createState() => ElevatedButtonStateAcamykids();
}

class ElevatedButtonStateAcamykids extends State<ElevatedButtonAcamykids> {

  late Offset _offset;
  bool _firstLoad = true;
  @override
  void initState() {
    super.initState();
    _offset = Offset(0, 0);
  }

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    Offset _defaultOffset = Offset(-responsive.ip(0.5), responsive.ip(0.35));
    if (_firstLoad) {
      _offset = _defaultOffset;
      _firstLoad = false;
    }
    
    return GestureDetector(
      onTap: () => widget.onPress(),
      onLongPressEnd: (value) {
        setState(() => _offset = _defaultOffset);
      },
      onTapDown: (value) {
        setState(() => _offset = Offset(-0, 0));
      },
      onTapUp: (value) {
        Timer(Duration(milliseconds: 20), () {
          setState(() => _offset = _defaultOffset);
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        width: widget.width ?? responsive.wp(20),
        height: widget.height ?? responsive.ip(6),
        child: Center(
          child: widget.child
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(responsive.ip(1.5)),
          color: widget.color,
          boxShadow: [
            BoxShadow(offset: _offset, color: widget.shadowColor),
          ],
        ),
      ),
    );
  }
}
