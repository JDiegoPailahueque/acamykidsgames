import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final Function onPressed;

  MainButton({
    required this.text,
    required this.icon,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive(context);
    final bool isMobile = Responsive.isMobile(context);
    
    return Container(
      constraints: (isMobile) 
      ? null
      : BoxConstraints(
        maxWidth: responsive.ip(23)
      ),
      padding: EdgeInsets.symmetric(vertical: responsive.ip(0.6)),
      child: MaterialButton(
        elevation: 0,
        onPressed: () => this.onPressed(),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(80.0),
        ),
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[
                Color(0xFF8622f2),
                Color(0xFFb946e7),
              ],
            ),
            borderRadius: BorderRadius.all(Radius.circular(80.0)),
          ),
          child: Container(
            padding: EdgeInsets.all(responsive.ip(1)),
            // constraints: BoxConstraints(minWidth: 88.0, minHeight: 36.0),
            alignment: Alignment.center,
            child: RichText(
              text: TextSpan(
                children: [
                  WidgetSpan(
                      child: Icon(this.icon,
                          size: responsive.ip(3), color: Colors.white)),
                  TextSpan(
                    text: "  " + this.text,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'FredokaOne',
                        fontSize: responsive.ip((isMobile) ? 3 : 2.6)),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
