import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/games/colors.dart';

class LinearProgress extends StatefulWidget {
  final double percent;
  final Color backgroundColor;
  final Color barColor;
  final double height;

  LinearProgress({
    required this.percent,
    this.backgroundColor = Colors.white,
    this.barColor = GameColor.primary,
    this.height = 35,
  });

  @override
  _LinearProgressState createState() => _LinearProgressState();
}

class _LinearProgressState extends State<LinearProgress>
    with SingleTickerProviderStateMixin {
  AnimationController? controller;
  double previousPercentage = 0.0;

  @override
  void initState() {
    previousPercentage = widget.percent;
    controller = new AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
    );

    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    controller?.forward(from: 0.0);

    final dif = widget.percent - previousPercentage;
    previousPercentage = widget.percent;

    return (controller != null)
      ? AnimatedBuilder(
        animation: controller!,
        builder: (BuildContext context, Widget? child) {
          return Container(
            padding: EdgeInsets.all(10),
            width: double.infinity,
            height: widget.height,
            child: CustomPaint(
              painter: _DrawLinearProgress(
                (widget.percent - dif) + (dif * controller!.value),
                widget.backgroundColor,
                widget.barColor,
                widget.height,
              ),
            ),
          );
        },
      ) : Container();
  }
}

class _DrawLinearProgress extends CustomPainter {
  final percent;
  final Color backgroundColor;
  final Color barColor;
  final double height;

  _DrawLinearProgress(
      this.percent, this.backgroundColor, this.barColor, this.height);

  @override
  void paint(Canvas canvas, Size size) {
    final background = new Paint()
      ..strokeWidth = height
      ..color = backgroundColor
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    final progress = new Paint()
      ..strokeWidth = height - 3
      ..color = barColor
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    final border = new Paint()
      ..strokeWidth = height + 3
      ..color = barColor
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    canvas.drawLine(
      Offset(0, size.height),
      Offset(size.width, size.height),
      border,
    );
    canvas.drawLine(
      Offset(0, size.height),
      Offset(size.width, size.height),
      background,
    );
    canvas.drawLine(
      Offset(0, size.height),
      Offset(size.width * (100 - percent) / 100, size.height),
      progress,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
