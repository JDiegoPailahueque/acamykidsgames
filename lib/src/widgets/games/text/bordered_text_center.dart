import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';

class BorderedTextCenter extends StatelessWidget {
  final String text;
  final double fontSize;
  final double strokeWidth;
  final List<Color> colors;
  
  BorderedTextCenter({
    required this.text,
    this.fontSize = 3,
    this.strokeWidth = 4.5,
    this.colors = const <Color>[
      Color(0xff5200FF),
      Color(0xd9FD28C1),
    ],
  });
  
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);

    final Shader linearGradient = LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: colors,
    ).createShader(
      Rect.fromLTWH(0, 0, responsive.wp(100) * 1.25, 0),
    );

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        // Stroked text as border.
        Text(
          text,
          style: TextStyle(
            fontFamily: 'FredokaOne',
            fontSize: responsive.ip(fontSize),
            foreground: Paint()
              ..shader = linearGradient
              ..style = PaintingStyle.stroke
              ..strokeWidth = strokeWidth,
            decoration: TextDecoration.none,
            letterSpacing: 1,
          ),
          textAlign: TextAlign.center,
        ),
        // Solid text as fill.
        Text(
          text,
          style: TextStyle(
            fontFamily: 'FredokaOne',
            color: Colors.white,
            fontSize: responsive.ip(fontSize),
            decoration: TextDecoration.none,
            letterSpacing: 1,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
