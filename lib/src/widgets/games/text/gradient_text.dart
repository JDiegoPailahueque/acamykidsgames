import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';

class GradientText extends StatelessWidget {
  GradientText({
    required this.text,
    required this.fontSize,
    this.textAlign = TextAlign.center,
    this.letterSpacing = 0.2,
    this.colors = const [
      Color(0xff5200FF),
      Color(0xd9FD28C1),
    ],
  });

  final String text;
  final double fontSize;
  final double letterSpacing;
  final TextAlign textAlign;
  final List<Color> colors;

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    return ShaderMask(
      shaderCallback: (bounds) => LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: colors,
      ).createShader(
        Rect.fromLTWH(0, 0, bounds.width * 1.75, bounds.height),
      ),
      child: Text(
        text,
        textAlign: textAlign,
        style: TextStyle(
          fontFamily: 'FredokaOne',
          color: Colors.white,
          fontSize: responsive.ip(fontSize),
          decoration: TextDecoration.none,
          letterSpacing: letterSpacing,
        ),
      ),
    );
  }
}
