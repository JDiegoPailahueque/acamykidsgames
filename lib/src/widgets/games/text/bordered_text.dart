import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';

class BorderedText extends StatelessWidget {
  final String text;
  final String fontFamily;
  final double fontSize;
  final double strokeWidth;
  final double wordSpacing;
  final double letterSpacing;
  final Color color;
  final List<Color> colors;
  final List<BoxShadow> shadows;
  final TextAlign textAlign;
  
  BorderedText({
    required this.text,
    this.fontFamily = 'FredokaOne',
    this.fontSize = 4.5,
    this.strokeWidth = 6,
    this.wordSpacing = 0,
    this.letterSpacing = 1.1,
    this.color = Colors.white,
    this.colors = const <Color>[
      Color(0xff5200FF),
      Color(0xd9FD28C1),
    ],
    this.shadows = const [],
    this.textAlign = TextAlign.start,
  });
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);

    final Shader linearGradient = LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: colors,
    ).createShader(
      Rect.largest
      // Rect.fromLTWH(0, 0, responsive.wp(100) * 1.25, 0),
    );

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        // Stroked text as border.
        Text(
          text,
          style: TextStyle(
            fontFamily: fontFamily,
            fontSize: responsive.ip(fontSize),
            foreground: Paint()
              ..shader = linearGradient
              ..style = PaintingStyle.stroke
              ..strokeWidth = strokeWidth,
            decoration: TextDecoration.none,
            letterSpacing: letterSpacing,
            wordSpacing: wordSpacing,
            shadows: shadows,
          ),
          textAlign: textAlign,
        ),
        // GradientText(text: text, fontSize: fontSize + 1),
        // Solid text as fill.
        Text(
          text,
          style: TextStyle(
            fontFamily: fontFamily,
            color: color,
            fontSize: responsive.ip(fontSize),
            decoration: TextDecoration.none,
            letterSpacing: letterSpacing,
            wordSpacing: wordSpacing,
          ),
          textAlign: textAlign,
        ),
      ],
    );
  }
}
