import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';

class GameHeader extends StatelessWidget {
  final String text;
  final double fontSize;
  final EdgeInsets? padding;

  GameHeader({
    required this.text,
    this.fontSize = 3.5,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    return Container(
      padding: padding ??
          EdgeInsets.only(
            left: responsive.ip(2),
            right: responsive.ip(2),
            top: responsive.ip(0.5),
            bottom: responsive.ip(2),
          ),
      width: double.infinity,
      color: Colors.white,
      child: GradientText(
        text: text,
        fontSize: fontSize,
      ),
    );
  }
}
