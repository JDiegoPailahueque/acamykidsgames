import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/utils/degrees_to_radius.dart';

class GameBackground extends StatelessWidget {
  final List<Widget> children;

  GameBackground({
    required this.children,
  });

  @override
  Widget build(BuildContext context) {
    bool portrait = MediaQuery.of(context).orientation == Orientation.portrait;
    final responsive = Responsive(context);
    return Stack(
      children: <Widget>[
        Container(
          width: responsive.wp(100.0),
          height: responsive.hp(100.0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment(3.0, 0.5),
              stops: [0, 1050],
              colors: [const Color(0xcc13A4F6), const Color(0xccC141FE)],
            ),
          ),
        ),
        if (portrait) portraitBG(responsive),
        if (!portrait) ...landscapeBG(responsive),
        ...children,
      ],
    );
  }

  Widget portraitBG(Responsive responsive) {
    return Positioned(
      left: responsive.wp(24),
      child: Container(
        child: Image.asset(
          'assets/games/game_bg.png',
          width: responsive.wp(100.0),
          height: responsive.hp(100.0),
        ),
      ),
    );
  }

  List<Widget> landscapeBG(Responsive responsive) {
    return [
      Positioned(
        left: responsive.wp(50),
        child: Image.asset(
          'assets/games/game_bg.png',
          width: responsive.wp(60.0),
          fit: BoxFit.fitWidth,
        ),
      ),
      Positioned(
        left: -responsive.wp(10),
        top: -responsive.wp(55),
        child: Transform.rotate(
          angle: degreesToRads(180),
          child: Image.asset(
            'assets/games/game_bg.png',
            width: responsive.wp(45),
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
    ];
  }
}
