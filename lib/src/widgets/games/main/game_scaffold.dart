import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/widgets/games/main/app_bar_games.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_background.dart';
import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';

class GameScaffold extends StatelessWidget {
  final int? score;
  final int totalTime;
  final Function endGame;
  final Widget builder;
  final String statement;
  final bool time;

  GameScaffold({
    this.score,
    required this.totalTime,
    required this.endGame,
    required this.builder,
    this.statement = "", 
    this.time = true,
  });

  @override
  Widget build(BuildContext context) {
    BaseGameModel baseGameModel = Provider.of<BaseGameModel>(context);
    return Scaffold(
      appBar: AppBarGames(
        appBar: AppBar(),
        score: score ?? 0,
        endGame: endGame,
        totalTime: totalTime,
        statement: statement,
        time: time,
        onPressLeading: () {
          baseGameModel.stopAudio();
          Navigator.pop(context);
        },
      ),
      body: GameBackground(
        children: [builder],
      ),
    );
  }
}
