import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';

class GameScoreWidget extends StatelessWidget {
  final Responsive responsive;
  final int score;
  
  GameScoreWidget({
    required this.responsive,
    required this.score,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(children: [
        Image.asset(
          'assets/games/star.png',
          height: 18,
        ),
        SizedBox(width: 10),
        GradientText(text: score.toString(), fontSize: 2.6),
      ]),
    );
  }
}
