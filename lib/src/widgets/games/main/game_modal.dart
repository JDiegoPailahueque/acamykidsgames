import 'package:acamykidsgames/src/models/games/question.dart';
import 'package:acamykidsgames/src/models/games_model.dart';
import 'package:acamykidsgames/src/pages/games/main/base_game_page.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/games/buttons/main_button.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class GameModal extends StatelessWidget {
  final Function closeModal;
  final Game game;

  const GameModal({super.key, 
    required this.closeModal,
    required this.game,
  });

  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive(context);
    final bool isMobile = Responsive.isMobile(context);

    return Container(
      color: Colors.black12,
      child: Align(
        child: Container(
            padding: EdgeInsets.symmetric(
              vertical: responsive.ip(1),
              horizontal: responsive.ip(2),
            ),
            constraints: BoxConstraints(maxWidth: responsive.ip((isMobile) ? 40.0 : 30.0)),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(responsive.ip(3)),
            ),
            width: responsive.wp(60),
            height: responsive.ip((isMobile) ? 32.0 : 27.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                MaterialButton(
                  elevation: 0,
                  color: Colors.white,
                  onPressed: () => closeModal(),
                  padding: EdgeInsets.all(0),
                  child: Text(
                    'Cerrar',
                    style: TextStyle(
                      fontFamily: 'FredokaOne',
                      fontSize: responsive.ip(2.2),
                      color: Colors.black,
                    ),
                  ),
                ),
                MainButton(
                  text: "Jugar",
                  icon: FontAwesomeIcons.gamepad,
                  onPressed: () => goGamePage(
                    context,
                    game,
                    0,
                  ),
                ),
              ],
            )),
      ),
    );
  }

  void goGamePage(BuildContext context, Game game, int asignaturaId) {
    closeModal();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => BaseGamePage(game, asignaturaId)),
    );
  }
}
