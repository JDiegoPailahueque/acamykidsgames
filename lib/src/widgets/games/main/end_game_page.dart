import 'package:acamykidsgames/src/models/games/question.dart';
import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/utils/orientation_app.dart';

import 'package:acamykidsgames/src/widgets/games/text/bordered_text.dart';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_background.dart';

class EndGamePage extends StatefulWidget {
  final int? score;
  final Function endGame;
  final String text;
  
  EndGamePage({
    this.score,
    required this.endGame,
    this.text = '¡Muy bien!',
  });

  @override
  _EndGamePageState createState() => _EndGamePageState();
}

class _EndGamePageState extends State<EndGamePage> {
  Image? leftCircle;
  Image? fireCracker;
  @override
  void initState() {
    super.initState();
    leftCircle = Image.asset("assets/arrow_left_circle.png");
    fireCracker = Image.asset("assets/games/fire_cracker.png");
  }

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: MaterialButton(
          child: Container(
            child: leftCircle,
          ),
          onPressed: () => exitGame()
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: BorderedText(
          text: 'AcamyKids',
          fontSize: 3,
        ),
      ),
      body: GameBackground(
        children: [
          Positioned(
            left: 0,
            bottom: 0,
            width: responsive.wp(orientation == Orientation.portrait ? 85 : 42),
            child: Image.asset("assets/games/cofre.png"),
          ),
          SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: responsive.ip(2)),
                      child: Text(
                        ((widget.score ?? 0) > 0)
                            ? widget.text
                            : '¡Vuelve a\nintentarlo!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'FredokaOne',
                          fontSize: responsive.ip(((widget.score ?? 0) > 0) ? 5 : 3.4),
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      height: responsive.ip(10),
                      child: fireCracker,
                    ),
                  ],
                ),
                scoreContainer(responsive, orientation),
                orientation == Orientation.portrait
                    ? Column(children: buttons(context))
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        textDirection: TextDirection.rtl,
                        children: buttons(context)
                          ..insert(0, SizedBox(width: responsive.ip(2))),
                      ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget scoreContainer(Responsive responsive, Orientation orientation) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: responsive.wp(orientation == Orientation.portrait ? 60 : 25),
          alignment: Alignment.center,
          margin: EdgeInsets.symmetric(vertical: (orientation == Orientation.portrait) ? responsive.ip(2) : 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                'assets/games/star2.png',
                height: responsive.ip(4.5),
              ),
              Container(
                padding: EdgeInsets.only(
                  left: responsive.ip(1),
                ),
                child: BorderedText(
                  text: '${widget.score}',
                  fontSize: 4,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  List<Widget> buttons(context) {
    Responsive responsive = Responsive(context);
    return [
      MaterialButton(
        color: Colors.white,
        shape: StadiumBorder(),
        padding: EdgeInsets.symmetric(
          vertical: responsive.ip(1),
          horizontal: responsive.ip(2),
        ),
        child: GradientText(text: "Reintentar", fontSize: 3),
        onPressed: () => widget.endGame(false),
      ),
      GestureDetector(
        child: Container(
          margin: EdgeInsets.symmetric(
            vertical: responsive.ip(1),
            horizontal: responsive.ip(3),
          ),
          padding: EdgeInsets.symmetric(
            vertical: responsive.ip(2),
            horizontal: responsive.ip(2),
          ),
          child: Text(
            'Volver al menú',
            style: TextStyle(
              fontFamily: 'FredokaOne',
              fontSize: responsive.ip(2.2),
              color: Colors.white,
            ),
          ),
        ),
        onTap: () => exitGame(),
      ),
    ];
  }

  void exitGame() {
    OrientationApp.setDefaultOrientation();
    // Provider.of<GameService>(context, listen: false).getGameStats();
    Navigator.pop(context);
  }
}
