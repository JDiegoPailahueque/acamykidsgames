import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/games/game_timer.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_score.dart';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';

class AppBarGames extends StatelessWidget implements PreferredSizeWidget {
  final AppBar appBar;
  final int score;
  final int totalTime;
  final Function endGame;
  final Function onPressLeading;
  final String statement;
  final bool time;

  const AppBarGames({
    required this.appBar,
    required this.score,
    required this.totalTime,
    required this.endGame,
    required this.onPressLeading,
    this.statement = '',
    this.time = true,
  });

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    final bool isLargeTablet = Responsive.isLargeTablet(context);

    return AppBar(
      elevation: 0,
      leading: MaterialButton(
        padding: EdgeInsets.all(preferredSize.height * 0.1),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: responsive.ip(0.4)),
          margin: EdgeInsets.only(left: responsive.ip(1)),
          child: Image.asset(
            'assets/games/close.png',
          ),
        ),
        onPressed: () => onPressLeading(),
      ),
      actions: <Widget>[
        SafeArea(
          child: Row(
            children: [
              SizedBox(width: responsive.wp(2)),
              GameScoreWidget(responsive: responsive, score: score),
              SizedBox(width: responsive.wp(1.5)),
              GameTimer(
                responsive: responsive,
                endGame: endGame,
                totalTime: totalTime,
                time: time,
              ),
              SizedBox(width: responsive.wp(2.5)),
            ],
          ),
        )
      ],
      backgroundColor: Colors.white,
      title: statement.length > 0
          ? Center(
              child: FittedBox(
                child: GradientText(
                  text: statement,
                  fontSize: isLargeTablet ? 1.65 : 2.4,
                ),
              ),
            )
          : Container(),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}
