import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/utils/games/colors.dart';
export 'package:acamykidsgames/src/utils/games/colors.dart';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';

class BasicButton extends StatefulWidget {
  final String text;
  final Color? color;
  final double fontSize;
  final Function onPressed;
  final bool disabled;
  final double? verticalPadding;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final GameColorStatus? status;
  final Widget? child;

  BasicButton({
    required this.onPressed,
    this.text = '',
    this.color,
    this.fontSize = 2,
    this.disabled = false,
    this.verticalPadding,
    this.padding = const EdgeInsets.symmetric(vertical: 4, horizontal: 24),
    this.margin = const EdgeInsets.all(0),
    this.status,
    this.child,
  });

  @override
  _BasicButtonState createState() => _BasicButtonState();
}

class _BasicButtonState extends State<BasicButton> {
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);
    return Expanded(
      child: Container(
        padding: widget.padding,
        margin: widget.margin,
        child: MaterialButton(
          disabledColor: Colors.grey,
          padding: EdgeInsets.symmetric(
            vertical: (widget.verticalPadding == null)
                ? 8
                : this.widget.verticalPadding!,
            horizontal: 16,
          ),
          shape: StadiumBorder(),
          color: widget.color ?? (widget.status != null ? getColorByStatus(widget.status!) : null),
          child: widget.child ??
              (widget.status == GameColorStatus.active
                  ? GradientText(
                    text: widget.text,
                    fontSize: widget.fontSize,
                  )
                  : Text(
                      widget.text,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'FredokaOne',
                        fontSize: responsive.ip(widget.fontSize),
                        color: Colors.white,
                      ),
                    )),
          onPressed: () => widget.onPressed(),
        ),
      ),
    );
  }
}
