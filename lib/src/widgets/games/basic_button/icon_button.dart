import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';

class BasicIconButton extends StatelessWidget {
  final Responsive responsive;
  final IconData icon;
  final Function onPressed;
  
  BasicIconButton({
    required this.responsive,
    required this.onPressed,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () => onPressed(),
      elevation: 2.0,
      fillColor: Colors.blue,
      child: Icon(
        icon,
        color: Colors.white,
        size: responsive.ip(3),
      ),
      padding: EdgeInsets.all(15.0),
      shape: CircleBorder(),
    );
  }
}
