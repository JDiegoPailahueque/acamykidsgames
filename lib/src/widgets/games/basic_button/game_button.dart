import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';

class GameButton extends StatelessWidget {
  final String text;
  final GameButtonStatus status;
  final EdgeInsets padding;
  final Function onPressed;
  final bool fitText;
  GameButton({
    required this.text,
    required this.status,
    required this.onPressed,
    this.padding = const EdgeInsets.symmetric(vertical: 25),
    this.fitText = false,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: StadiumBorder(),
      elevation: 0,
      color: _getButtonColorByStatus(status),
      padding: padding,
      child: fitText
          ? FittedBox(
              fit: BoxFit.contain,
              child: GradientText(
                text: text,
                fontSize: 2.5,
                colors: _getTextColorsByStatus(status),
              ),
            )
          : GradientText(
              text: text,
              fontSize: 2.5,
              colors: _getTextColorsByStatus(status),
            ),
      onPressed: () => onPressed(),
    );
  }

  Color _getButtonColorByStatus(GameButtonStatus status) {
    if (status == GameButtonStatus.active) {
      return Colors.white;
    } else if (status == GameButtonStatus.success) {
      return Color(0xCC3B966A);
    } else if (status == GameButtonStatus.error) {
      return Color(0x80FF2727);
    } else {
      return Colors.white.withAlpha(120);
    }
  }
}

List<Color> _getTextColorsByStatus(GameButtonStatus status) {
  if (status == GameButtonStatus.active) {
    return [Color(0xff5200FF), Color(0xd9FD28C1)];
  } else {
    return [Colors.white, Colors.white];
  }
}

enum GameButtonStatus { active, inactive, success, error, disabled }
