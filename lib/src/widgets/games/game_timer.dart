import 'dart:async';
import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';

class GameTimer extends StatefulWidget {
  final Responsive responsive;
  final int totalTime;
  final Function endGame;
  final bool time;

  GameTimer({
    required this.responsive,
    required this.totalTime,
    required this.endGame,
    this.time = true,
  });

  @override
  _GameTimerState createState() => _GameTimerState();
}

class _GameTimerState extends State<GameTimer> {
  int _currentTime = 0;
  bool stopTimer = false;

  @override
  void initState() {
    super.initState();
    _currentTime = widget.totalTime;
    if (widget.time) {
      Future.microtask(() => setTime());
    }
  }

  @override
  void dispose() {
    stopTimer = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.time,
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset('assets/games/game_timer.png', height: 24),
            SizedBox(width: 10),
            GradientText(
              text: currentTime.toString(),
              fontSize: 2.5,
              colors: [
                Color(0xff13A4F6),
                Color(0xffC141FE),
              ],
            ),
          ],
        ),
      ),
    );
  }

  String get currentTime => _currentTime >= 10 ? '$_currentTime' : '0$_currentTime';

  void setTime() async {
    const second = const Duration(seconds: 1);
    new Timer.periodic(second, (Timer timer) {
      if (_currentTime == 0 || stopTimer) {
        timer.cancel();
        endGame();
      } else {
        setState(() {
          _currentTime--;
        });
      }
    });
  }

  void endGame() {
    if (_currentTime == 0) {
      widget.endGame(true);
    }
  }
}
