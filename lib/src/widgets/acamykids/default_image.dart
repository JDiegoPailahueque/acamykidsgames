import 'package:acamykidsgames/src/widgets/games/text/bordered_text.dart';
import 'package:flutter/material.dart';

class DefaultImage extends StatelessWidget {
  final double fontSize;
  final double strokeWidth;

  DefaultImage({
    Key? key,
    this.fontSize = 3.5,
    this.strokeWidth = 6,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.maxFinite,
      width: double.maxFinite,
      child: BorderedText(
        text: 'AcamyKids',
        fontSize: fontSize,
        strokeWidth: strokeWidth,
      ),
    );
  }
}