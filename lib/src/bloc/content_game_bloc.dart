
import 'package:acamykidsgames/src/models/games_model.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

class ContentGameBloc with ChangeNotifier {
  static final ContentGameBloc i = ContentGameBloc._();

  ContentGameBloc._();

  List<Unit>? _gameUnits = [];

  List<Unit> get gameUnits => _gameUnits ?? [];
  List<Game> get games => gameUnits.expand((x) => x.games).expand((x) => x) .toList();

  set gameUnits(List<Unit>? data) {
    _gameUnits = data;
    notifyListeners();
  }



  void refreshUnitGames() {
    if (_gameUnits != null) {
      gameUnits = gameUnits;
    }
  }

  Game? getGameContent({required int gameId}) {
    return games.firstWhereOrNull((gc) => gc.id == gameId);
  }

  

  void refreshGameContents({required List<Game> data}) {
    if (games.isNotEmpty) {
      for (Game game in games) {
        Game? gFound = data.firstWhereOrNull((g) => g.id == game.id);

        if (gFound != null) {
          game.status = gFound.status;
        }
      }

      refreshUnitGames();
    }
  }

  void refreshGames() {
    if (games.isNotEmpty) {
      _gameUnits = _gameUnits;
    }
  }
}
