class TypeTabContent {
  final int? _value;

  const TypeTabContent(int value) : _value = value;

  int get value => _value ?? 0;

  toString() => 'TypeTabContent($_value)';

  static TypeTabContent from(int value) => TypeTabContent(value);

  static const home = const TypeTabContent(0);
  static const evaluation = const TypeTabContent(1);
  static const game = const TypeTabContent(2);
  static const lesson = const TypeTabContent(3);
  static const reading = const TypeTabContent(4);
  static const activity = const TypeTabContent(5);
  static const ia = const TypeTabContent(6);
  static const notification = const TypeTabContent(7);
  static const config = TypeTabContent(8);
  static const search = const TypeTabContent(9);

}
