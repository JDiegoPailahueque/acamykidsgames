import 'package:acamykidsgames/src/models/type_content.dart';

class ContentAc {
  int id;
  String name;
  int? order;
  int status;
  double progress;
  TypeContent typeContent;
  int index;
  bool hasBeenCompleted;

  ContentAc({
    this.id = 0,
    this.order,
    this.name = '',
    this.status = 7,
    this.progress = 0.0,
    required this.typeContent,
    this.index = -2,
    this.hasBeenCompleted = false
  });

}