import 'package:flutter/material.dart';

class ContentStyle {
  final Color primary;
  final Color secondary;
  final Color tertiary;
  final Color quaternary;
  final Color quinary;
  final Color senary;
  final Color septary;
  final Color octonary;

  ContentStyle({
    required this.primary,
    required this.secondary,
    required this.tertiary,
    required this.quaternary,
    this.quinary = Colors.black,
    this.senary = Colors.black,
    this.septary = Colors.black,
    this.octonary = Colors.black,
  });
}