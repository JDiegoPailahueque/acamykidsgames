import 'dart:convert';

import 'package:acamykidsgames/src/models/content/content_ac_model.dart';
import 'package:acamykidsgames/src/models/file/image_ak.model.dart';
import 'package:acamykidsgames/src/models/type_content.dart';
import 'package:flutter/material.dart';


Map<int, Color> primaryColors = {
  1: Color(0xff40B97F),
  2: Color(0xffF89437),
  3: Color(0xff3294F9),
  4: Color(0xffF7429E),
  5: Color(0xffE64D26),
  6: Color(0xff947986),
};
Map<int, Color> secondaryColors = {
  1: Color(0xff3B966A),
  2: Color(0xffF89437),
  3: Color(0xff27A5FF),
  4: Color(0xffFA37A7),
  5: Color(0xffE74E23),
  6: Color(0xff907D86),
};

class SubjectGames {
  String name;
  String image;
  int? level;
  int asignaturaId;
  List<Unit> units;

  SubjectGames({
    required this.name,
    required this.image,
    required this.level,
    required this.asignaturaId,
    required this.units,
  });

  List<Game> get games {
    List<List<Game>> unitGames = units.expand((x) => x.games).toList();
    return unitGames.expand((x) => x).toList();
  }

  factory SubjectGames.fromJson(Map<String, dynamic> json, List<Unit> units) {
    return SubjectGames(
      name: json['materia_descripcion'],
      image: json['image'],
      level: json['nivel_id'],
      asignaturaId: json['id'],
      units: units,
    );
  }
}

class Unit {
  String id;
  int unidadId;
  int unidadNombre;
  List<List<Game>> games;
  List<int> gameIds;
  int? subjectId;
  int? courseId;

  Unit({
    this.id = '',
    this.unidadId = 1,
    required this.unidadNombre,
    this.games = const [],
    this.gameIds = const [],
    this.subjectId,
    this.courseId
  });

  Color get color {
    return primaryColors[unidadNombre] ?? Color(0xff262626);

  }

  Color get secondaryColor {
    return secondaryColors[unidadNombre] ?? Color(0xff262626).withOpacity(0.8);
  }

  factory Unit.fromDBJson(Map<String, dynamic> jsonDb) {
    return Unit(
      id: jsonDb['id'],
      unidadId: jsonDb['unidad_id'],
      unidadNombre: jsonDb['unidad_nombre'],
      games: [],
      gameIds: jsonDb['game_ids'] != null ? List<int>.from(json.decode(jsonDb['game_ids'])) : [],
    );
  }

  factory Unit.fromJson(Map<String, dynamic> json) {
    Unit u = Unit(
      unidadId: json['grupo_id'],
      unidadNombre: json['unidad_nombre'],
      games: List<List<Game>>.from(
        json['juegos'].map(
          (list) => List<Game>.from(
            (list.map((y) => Game.fromJson(y))),
          ),
        ),
      ),
    );
    u.gameIds = u.games.expand((x) => x).map((g) => g.gameId).toList();

    return u;
  }

  factory Unit.fromJsonIds(Map<String, dynamic> json) => Unit(
    unidadId: json['grupo_id'],
    unidadNombre: json['unidad_nombre'],
    subjectId: json['asignatura_id'],
    courseId: json['curso_id'],
    gameIds: (json['game_ids'] != null) ? List<int>.from(json['game_ids']) : [],
  );


  Map<String, dynamic> toJson({
    required int asignaturaId,
    required int cursoId,
  }) {
    return {
      'id': "$cursoId$asignaturaId$unidadId",
      'unidad_id': unidadId,
      'unidad_nombre': unidadNombre,
      'asignatura_id': asignaturaId,
      'curso_id': cursoId,
      'game_ids': json.encode(gameIds),
    };
  }

  Map<String, dynamic> toJsonIds() => {
    'id': id,
    'unidad_id': unidadId,
    'unidad_nombre': unidadNombre,
    'asignatura_id': subjectId,
    'curso_id': courseId,
    'game_ids': json.encode(gameIds),
  };
}

class Game extends ContentAc {
  int gameId;
  String gameCode;
  String gameName;
  String gamePage;
  bool landscape;
  String tag;
  ImageAK? image;

  Game({
    required this.gameId,
    required this.gameCode,
    required this.gameName,
    required this.gamePage,
    required this.landscape,
    this.tag = '',
    this.image,
  })
  : super(
    id: gameId,
    name: gameName,
    typeContent: const TypeContent(7),
  );

  factory Game.fromJson(Map<String, dynamic> json) => Game(
    gameId: json['juego_id'],
    gameName: json['juego_nombre'],
    gamePage: json['juego_pagina'] ?? '',
    gameCode: json['juego_codigo'],
    landscape: json['landscape'] == 1,
    tag: json['tag'] ?? '',
    image: (json['cover_img'] == null) ? null : ImageAK(id: json['juego_id'], url: json['cover_img']),
  );

  factory Game.fromJsonDb(Map<String, dynamic> json) => Game(
    gameId: json['juego_id'],
    gameName: json['juego_nombre'],
    gamePage: json['juego_pagina'] ?? '',
    gameCode: json['juego_codigo'],
    landscape: json['landscape'] == 1,
    tag: json['tag'] ?? '',
    image: (json['url_img'] == null) ? null : ImageAK(id: json['juego_id'], url: json['url_img'], urlDld: json['url_img_dld']),
  );

  Map<String, dynamic> toJson() {
    return {
      'juego_id': gameId,
      'juego_nombre': gameName,
      'juego_pagina': gamePage,
      'juego_codigo': gameCode,
      'landscape': landscape ? 1 : 0,
      'tag': tag,
      'url_img': image?.url,
      'url_img_dld': image?.urlDld,
    };
  }
}

class GameStats {
  int gameId;
  int studentId;
  int courseId;
  int subjectId;
  int bestPuntaje;
  int ranking;

  GameStats({
    required this.gameId,
    required this.studentId,
    required this.courseId,
    required this.subjectId,
    required this.bestPuntaje,
    required this.ranking,
  });

  factory GameStats.fromJson(Map<String, dynamic> json) {
    return GameStats(
      gameId: json['juego_id'],
      studentId: json['estudiante_id'],
      courseId: json['curso_id'],
      subjectId: json['asignatura_id'],
      bestPuntaje: json['puntaje_mayor'],
      ranking: json['ranking'],
    );
  }

  factory GameStats.fromDBJson(Map<String, dynamic> json) {
    return GameStats(
      gameId: json['game_id'],
      studentId: json['student_id'],
      courseId: json['course_id'],
      subjectId: json['subject_id'],
      bestPuntaje: json['best_puntaje'],
      ranking: json['ranking'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'game_id': gameId,
      'student_id': studentId,
      'course_id': courseId,
      'subject_id': subjectId,
      'best_puntaje': bestPuntaje,
      'ranking': ranking,
    };
  }
}
