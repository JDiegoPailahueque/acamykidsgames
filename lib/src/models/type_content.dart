class TypeContent {
  final int? _value;

  const TypeContent(int value) : _value = value;

  int get value => _value ?? 0;

  toString() => 'TypeContent($_value)';

  static TypeContent from(int value) => TypeContent(value);

  static const multimedia = TypeContent(0);
  static const repository = TypeContent(1);
  static const video = TypeContent(2);
  static const audio = TypeContent(3);
  static const guide = TypeContent(4);
  static const bloque = TypeContent(5);
  static const book = TypeContent(6);
  static const game =(7);
  static const img = TypeContent(8);
  static const reviewBloque = TypeContent(9);
  static const audioTitle = TypeContent(10);
  static const extraGames = TypeContent(11);
  static const trainingGames = TypeContent(12);
  static const reward = TypeContent(13);
  static const audioBook = TypeContent(14);
  static const evaluation = TypeContent(15);
  static const activity = TypeContent(16);
}
