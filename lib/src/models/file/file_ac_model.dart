import 'package:acamykidsgames/src/models/type_content.dart';

class FileAc {
  int id;
  int status;
  double progress;
  TypeContent typeFile;
  String url;
  String? urlDld;

  FileAc({
    required this.id,
    this.status = 7,
    this.progress = 0.0,
    required this.typeFile,
    required this.url,
    this.urlDld,
  });
}