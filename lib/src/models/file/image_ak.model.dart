import 'package:acamykidsgames/src/models/file/file_ac_model.dart';
import 'package:acamykidsgames/src/models/type_content.dart';

class ImageAK extends FileAc {

  ImageAK({
    required int id,
    int status = 7,
    double progress = 0.0,
    TypeContent typeFile = TypeContent.img,
    required String url,
    String? urlDld,
  }) : super(
    id: id,
    status: status,
    progress: progress,
    typeFile: TypeContent.img,
    url: url,
    urlDld: urlDld,
  );

}