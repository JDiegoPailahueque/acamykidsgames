class Fraction {
  int mixedNumber;
  int numerator;
  int denominator;
  int state;

  Fraction(
    this.mixedNumber,
    this.numerator,
    this.denominator, {
    this.state = 0,
  });
}
