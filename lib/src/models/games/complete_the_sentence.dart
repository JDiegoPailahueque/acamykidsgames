class SentenceItem {
  final List<String> wordsTargetList;
  final String sentence;
  bool status;

  SentenceItem({
    required this.wordsTargetList,
    required this.sentence,
    this.status = false,
  });
}
