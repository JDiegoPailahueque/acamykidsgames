class LSQuestion {
  String statement;
  List<String> alternatives;
  String correctAnswer;

  LSQuestion({
    required this.statement,
    required this.alternatives,
    required this.correctAnswer,
  });
}

LSQuestion setQuestion({
  String statement = '',
  required List<String> alternatives,
  required String correctAnswer,
}) {
  alternatives = alternatives..shuffle();
  int total = alternatives.length >= 3 ? 3 : alternatives.length;
  return LSQuestion(
    statement: statement,
    alternatives: List.generate(total, (i) => alternatives[i])
      ..add(correctAnswer)
      ..shuffle(),
    correctAnswer: correctAnswer,
  );
}
