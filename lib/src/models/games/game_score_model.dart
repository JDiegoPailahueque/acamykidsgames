class GameScore {
  int juegoId;
  int asignaturaId;
  int estudianteId;
  int puntaje;

  GameScore({
    required this.juegoId,
    required this.asignaturaId,
    required this.estudianteId,
    required this.puntaje,
  });

  factory GameScore.fromJson(Map<String, dynamic> json) => GameScore(
      juegoId: json['juego_id'],
      asignaturaId: json['asignatura_id'],
      estudianteId: json['estudiante_id'],
      puntaje: json['puntaje'],
  );

  Map<String, dynamic> toJson() => {
    'juego_id': juegoId,
    'asignatura_id': asignaturaId,
    'estudiante_id': estudianteId,
    'puntaje': puntaje,
  };
}