import 'package:flutter/material.dart';
export 'package:flutter/material.dart';
import 'package:acamykidsgames/src/widgets/games/headers/game_header.dart';

class Question {
  List<Widget> children;
  List<String> alternatives;
  String correctAnswer;
  GameHeader? header;
  String? image;

  Question({
    required this.children,
    this.alternatives = const [],
    required this.correctAnswer,
    required this.header,
    this.image,
  });
}

Question setQuestion({
  List<Widget> children = const [],
  List<String> alternatives = const [],
  required String correctAnswer,
  GameHeader? header,
  String? image,
}) {
  alternatives = alternatives..shuffle();
  int total = alternatives.length >= 3 ? 3 : alternatives.length;
  return Question(
    children: children,
    alternatives: List.generate(total, (i) => alternatives[i])
      ..add(correctAnswer)
      ..shuffle(),
    correctAnswer: correctAnswer,
    header: header,
    image: image,
  );
}
