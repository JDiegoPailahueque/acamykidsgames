class Statement {
  String title;
  String? img;
  bool response;

  Statement({
    required this.title,
    this.img,
    required this.response,
  });
}