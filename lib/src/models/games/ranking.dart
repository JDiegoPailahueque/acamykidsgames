class RankingModel {
  int estudianteId;
  int puntaje;
  String personaNombre;
  String personaApellido;

  RankingModel({
    this.estudianteId = 0,
    this.puntaje = 0,
    this.personaNombre = "",
    this.personaApellido = "",
  });

  factory RankingModel.fromJson(Map<String, dynamic> json) => RankingModel(
    estudianteId: int.tryParse('${json["estudiante_id"] ?? 0}') ?? 0,
    puntaje: int.tryParse('${json["puntaje"] ?? 0}') ?? 0,
    personaNombre:
        json["persona_nombre"].toString().split(' ')[0][0].toUpperCase() +
            json["persona_nombre"].toString().split(' ')[0].substring(1),
    personaApellido:
        json["persona_apellido"].toString().split(' ')[0][0].toUpperCase() +
            json["persona_apellido"].toString().split(' ')[0].substring(1),
  );

  Map<String, dynamic> toJson() => {
    "estudiante_id": estudianteId,
    "puntaje": puntaje,
    "persona_nombre": personaNombre,
    "persona_apellido": personaApellido,
  };
}

class RankingJuegoModel {
  int estudianteId;
  int puntajeMayor;
  int puntajeSemanal;
  String personaNombre;
  String personaApellido;

  RankingJuegoModel({
    this.estudianteId = 0,
    this.puntajeMayor = 0,
    this.puntajeSemanal = 0,
    this.personaNombre = "",
    this.personaApellido = "",
  });

  factory RankingJuegoModel.fromJson(Map<String, dynamic> json) =>
      RankingJuegoModel(
        estudianteId: json["estudiante_id"],
        puntajeMayor: json["puntaje_mayor"],
        puntajeSemanal: json["puntaje_semanal"],
        personaNombre:
            json["persona_nombre"].toString().split(' ')[0][0].toUpperCase() +
                json["persona_nombre"].toString().split(' ')[0].substring(1),
        personaApellido:
            json["persona_apellido"].toString().split(' ')[0][0].toUpperCase() +
                json["persona_apellido"].toString().split(' ')[0].substring(1),
      );

  Map<String, dynamic> toJson() => {
        "estudiante_id": estudianteId,
        "puntaje_mayor": puntajeMayor,
        "puntaje_semanal": puntajeSemanal,
        "persona_nombre": personaNombre,
        "persona_apellido": personaApellido,
      };
}
