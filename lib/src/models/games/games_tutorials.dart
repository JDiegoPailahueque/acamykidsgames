class GameTutorial {
  int juegoId;
  int estudianteId;

  GameTutorial({
    required this.juegoId,
    required this.estudianteId,
  });

  factory GameTutorial.fromJson(Map<String, dynamic> json) => GameTutorial(
    juegoId: json['game_id'],
    estudianteId: json['estudiante_id'],
  );

  Map<String, dynamic> toJson() => {
    'game_id': juegoId,
    'estudiante_id': estudianteId,
  };
}