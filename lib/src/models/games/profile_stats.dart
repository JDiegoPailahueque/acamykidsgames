class ProfileGameStats {
  int count;
  String puntaje;
  String asignaturaName;

  ProfileGameStats({
    this.count = 0,
    this.puntaje = "0",
    this.asignaturaName = "",
  });

  factory ProfileGameStats.fromJson(Map<String, dynamic> json) => ProfileGameStats(
        count: json["count"],
        puntaje: json["puntaje"].toString(),
        asignaturaName: json["asignatura_name"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "puntaje": puntaje,
        "asignatura_name": asignaturaName,
      };
}
