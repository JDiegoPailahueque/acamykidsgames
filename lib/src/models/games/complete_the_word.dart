class MTargetItem {
  final int? data;
  final String? audio;
  final String? image;
  final int targetIndex;
  final List<String> children;

  MTargetItem({
    this.data,
    this.audio,
    this.image,
    required this.targetIndex,
    required this.children,
  });
}
