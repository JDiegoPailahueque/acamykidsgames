import 'dart:async';
import 'package:acamykidsgames/src/utils/textTransform.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
export 'package:provider/provider.dart';

class BaseGameModel with ChangeNotifier {
  static final BaseGameModel bg = BaseGameModel._();
  BaseGameModel._();

  // Puntos
  int _score = 0;
  double maxVolume = 0.3;

  // Audio Fonod
  bool _isPlaying = false;
  AudioPlayer? _player;

  // Audio Respuesta
  AudioPlayer? _responsePlayer;
  AudioPlayer? get responsePlayer => this._responsePlayer;

  int get score => this._score;
  set score(int newScore) {
    // No hay puntajes negativos
    if (newScore < 0)
      this._score = 0;
    else
      this._score = newScore;
  }

  bool get isPlaying => this._isPlaying;
  set isPlaying(bool status) => this._isPlaying = status;

  Future<void> play() async {
    _player ??= AudioPlayer();
    _isPlaying = true;

    await _player!.setAsset('assets/audios/fondo.mp3');
    _player!.setLoopMode(LoopMode.one);
    _player!.setVolume(maxVolume);
    _player!.play();
    notifyListeners();
  }

  setVolume(double volume) {
    _player?.setVolume(volume);
  }

  resume() {
    _player?.play();
  }

  pauseAudio() {
    _player?.pause();
  }

  stopAudio() {
    _player?.stop();
    _responsePlayer?.stop();
  }

  disposeAudio() {
    _isPlaying = false;
    _player?.stop();
    _responsePlayer?.stop();
  }
  
  _disposeResponsePlayer() {
    _responsePlayer?.stop();
    _responsePlayer = null;
  }

  Future<void> correctAnswer() async {
    await _playAudioResponse('chest.wav');
  }

  Future<void> wrongAnswer() async {
    await _playAudioResponse('error.mp3');
  }

  Future<void> _playAudioResponse(String assetAudio) async {
    _disposeResponsePlayer();
    _responsePlayer ??= AudioPlayer();

    await _responsePlayer!.setAsset('assets/audios/$assetAudio');
    _responsePlayer!.setVolume(0.8);
    _responsePlayer!.play();
  }

  Future<void> loadAudio(String? str, {double? newVolumen, bool clearStr = false}) async {
    if (str == null) return;
    if (clearStr) str = TextTransform.audioClearText(str);

    _player?.setVolume(newVolumen ?? 0.18);
    
    _disposeResponsePlayer();
    _playAudioResponse('statements/$str.mp3');

    _responsePlayer?.playerStateStream.listen((state) {
      if (state.processingState == ProcessingState.completed) {
        _player?.setVolume(maxVolume);
      }
    });
  }

  Future<void> loadAudios(List<String> audios, {bool clearStr = false}) async {
    _disposeResponsePlayer();

    String audioStr = audios[0];
    if (clearStr) audioStr = TextTransform.audioClearText(audioStr);
    
    _player?.setVolume(0.18);
    
    _playAudioResponse('statements/$audioStr.mp3');

    _responsePlayer?.playerStateStream.listen((state) {
      if (state.processingState == ProcessingState.completed) {
        if (audios.length == 1) {
          _player?.setVolume(maxVolume);
        } else {
          loadAudios(audios.sublist(1));
        }
      }
    });
  }
}
