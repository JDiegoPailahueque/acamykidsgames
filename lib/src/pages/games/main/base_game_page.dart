import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/services/game/games_service.dart';

import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
import 'package:acamykidsgames/src/utils/orientation_app.dart';
import 'package:acamykidsgames/src/pages/games/main/pages/loading_page.dart';
import 'package:acamykidsgames/src/models/games_model.dart';
import 'package:wakelock_plus/wakelock_plus.dart';
import '../math_first/_games.dart';

class BaseGamePage extends StatefulWidget {
  static String routeName = 'base_game_page';
  final Game game;
  final int? asignaturaId;

  const BaseGamePage(this.game, this.asignaturaId, {super.key});

  @override
  _BaseGamePageState createState() => _BaseGamePageState();
}

class _BaseGamePageState extends State<BaseGamePage>
    with WidgetsBindingObserver {
  @override
  void initState() {
    WakelockPlus.enable();
    WidgetsBinding.instance.addObserver(this);
    BaseGameModel.bg.score = 0;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    WakelockPlus.disable();
    WidgetsBinding.instance.removeObserver(this);
    OrientationApp.setDefaultOrientation();
    BaseGameModel.bg.disposeAudio();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      BaseGameModel.bg.pauseAudio();
    }

    if (state == AppLifecycleState.resumed) {
      BaseGameModel.bg.resume();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.game.landscape) {
      OrientationApp.setLandscapeOrientationLock();
    } else {
      OrientationApp.setPortraitOrientationLock();
    }

    Provider.of<GameService>(context).game = widget.game;

    return LoadingPage(
      child: getCurrentGame(),
    );
  }

  Widget getCurrentGame() {
    Map<String, Widget> games = {
      "numbers_one_to_nine": NumbersOneToNinePage(),
      "the_zero": TheZeroPage(),
      "the_ten": TheTenPage(),
      "compare_collections": CompareCollectionsPage(),
      "addition": AdditionPage(),
    };

    if (games[widget.game.gamePage] == null) return Container();
    return games[widget.game.gamePage]!;
  }
}
