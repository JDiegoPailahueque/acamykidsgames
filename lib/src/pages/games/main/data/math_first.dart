import 'package:acamykidsgames/src/models/games_model.dart';

SubjectGames mathFirst(bool isMatte) {
  return SubjectGames(
    name: "Matemática",
    image: "math.png",
    level: 1,
    asignaturaId: 1,
    units: [
      Unit(
        unidadNombre: 1,
        games: [
          [
            Game(
              gameId: 181,
              gameCode: "MATE1001",
              gameName: "Números del 1 al 9",
              gamePage: "numbers_one_to_nine",
              landscape: true,
              tag: (!isMatte) ? "SUMO PRIMERO" : '',
            ),
            Game(
              gameId: 182,
              gameCode: "MATE1002",
              gameName: "El número 0",
              gamePage: "the_zero",
              landscape: true,
            ),
          ],
          [
            Game(
              gameId: 183,
              gameCode: "MATE1003",
              gameName: "La decena",
              gamePage: "the_ten",
              landscape: true,
              tag: (!isMatte) ? "SUMO PRIMERO" : '',
            ),
            Game(
              gameId: 184,
              gameCode: "MATE1004",
              gameName: "Comparar Colecciones",
              gamePage: "compare_collections",
              landscape: true,
            ),
          ],
          [
            Game(
              gameId: 185,
              gameCode: "MATE1005",
              gameName: "La adición",
              gamePage: "addition",
              landscape: true,
            ),
          
          ],
          
        ],
      ),
    ],
  );
}
