import 'package:acamykidsgames/src/models/games_model.dart';

import 'package:acamykidsgames/src/pages/games/main/data/math_first.dart';

/* Tutorials */
import 'package:acamykidsgames/src/pages/games/main/data/tutorials/math_first.dart';


SubjectGames switchGames(String id) {
  switch (id) {
    case "MAT1":
      return mathFirst(false);
    // ,,, others
    default:
      return mathFirst(false);
  }
}

SubjectGames? getSubjectGamesMap(int subjectId) {
  bool isMatte = false;

  Map<int, SubjectGames> subjectGames = {
    1: mathFirst(isMatte),
    // ,,, others
  };
  return subjectGames[subjectId];
} 

List<String?> switchTutorialGames(String gameCode) {
  String subject = gameCode.substring(0, 5);
  // MATE
  switch (subject) {
    case "MATE1":
      return mathFirstTutorials(gameCode);
    // ,,, others
    default:
      return mathFirstTutorials(gameCode);
  }
}
