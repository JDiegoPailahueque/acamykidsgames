List<String?> mathFirstTutorials(String gameCode) {
  switch (gameCode) {
    case "MATE1001":
      return [
        'Observa la imagen y selecciona la alternativa que tenga la cantidad de monos.',
        'Tiempo límite: 100 s',
        null,
      ];
    case "MATE1002":
      return [
        'Arrastra las canastas que no tengan huevos al cero.',
        'Tiempo límite: 90 s',
        null,
      ];
    case "MATE1003":
      return [
        'Identifica cuantos platenas faltan para completar la decena.',
        'Selecciona la alternativa que corresponda al número de planetas faltantes ',
        'Tiempo límite: 90 s',
        null,
      ];
    case "MATE1004":
      return [
        'Arrastra los bloques a los circulos blancos segun el orden indicado.',
        'Tiempo límite: 80 s',
        null,
      ];
    case "MATE1005":
      return [
        'Suma la cantida de goles que anotaron Adrian y Luis.',
        'Selecciona la aternativa con la cantidad total de goles.',
        'Tiempo límite: 100 s',
        null,
      ];
    case "MATE1007":
      return [
        'Calcula la cantidad de puntos que se obtienen restando los del equipo con el jugador indicado por el enunciado.',
        'Selecciona la aternativa con la cantidad de puntos resultante.',
        'Tiempo límite: 120 s',
        null,
      ];
    case "MATE1008":
      return [
        'Ubica  y selecciona la alternativa con la posicion del pinguino indicado por el enunciado.',
        'Tiempo límite: 100 s',
        null,
      ];
    case "MATE1009":
      return [
        'Calcula el piso al que la pareja llegarán dependiendo del piso actual y cuantos deben subir o bajar.',
        'Seleccion la alternativa con el piso resultante',
        'Tiempo límite: 120 s',
        null,
      ];
    case "MATE1010":
      return [
        'Selecciona la alternativa con la posicion espacial del objeto indicado por el enunciado.',
        'Tiempo límite: 80 s',
        null,
      ];
    case "MATE1011":
      return [
        'Etapa 1: Selecciona el objeto mas corto o largo segun indique el enunciado.',
        'Etapa 2: Selecciona el jugador mas alto o bajo segun lo indique el enunciado.',
        'Tiempo límite: 80 s',
        null,
      ];
    case "MATE1012":
      return [
        'Selecciona una alternativa con la respuesta a la pregunta indicada por el enunciado, observando el calendario y el dia de referencia.',
        'Tiempo límite: 120 s',
        null,
      ];
    case "MATE1013":
      return [
        'Selecciona la alternativa con la cantidad de una misma figura indicada por el enunciado.',
        'Tiempo límite: 100 s',
        null,
      ];
    case "MATE1016":
      return [
        'Etapa 1: Compone la decena y unidad segun los numeros mostrados, arrastrando los números a los signos de interrogacion correspondientes.',
        'Etapa 2: Descompone la decena y unidad en números, arrastrando los números a los signos de interrogacion correspondientes.',
        'Tiempo límite: 80 s',
        null,
      ];
    case "MATE1020":
      return [
        'Completa el patron segun los numeros que muestran arriba de las casas.',
        'Arrastra los numeros faltantes a los signos de interrogaciones.'
            'Tiempo límite: 90 s',
        null,
      ];
    case "MATE1021":
      return [
        'Etapa 1: Selecciona una alternativa sobre la igualdad de los grupos de peces.',
        'Etapa 2: Selecciona la alternativa con la cantidad de peces falntantes para que los grupos sean iguales.',
        'Tiempo límite: 100 s',
        null,
      ];
    case "MATE1022A":
      return [
        'Selecciona la alternativa con la cantidad de preferencias del país que se indica en el enunciado .',
        'Tiempo límite: 100 s',
        null,
      ];
    case "MATE1022B":
      return [
        'Calcula la cantidad de veces que se repiten los tipos de climas segun se muestran en la semana.',
        'Utiliza los botones de + y - para indicar el conteo.',
        'Tiempo límite: 100 s',
        null,
      ];
    case "MATE1031A":
      return [
        'Selecciona la alternativa con la cantidad de preferencias del país que se indica en el enunciado .',
        'Tiempo límite: 100 s',
        null,
      ];
    case "MATE1031B":
      return [
        'Completa el pictograma agregando o quitando aviones a cada uno de los paises, guiate por la tabla de la izquierda para completarlo.',
        'Utiliza los botones de + y - para agregar o quitar aviones.',
        'Tiempo límite: 100 s',
        null,
      ];
    default:
      return [];
  }
}
