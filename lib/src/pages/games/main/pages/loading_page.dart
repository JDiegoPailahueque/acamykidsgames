import 'dart:async';
import 'package:acamykidsgames/src/services/game/games_service.dart';
import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/models/games_model.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_background.dart';
import 'package:acamykidsgames/src/widgets/games/text/bordered_text.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
import 'package:acamykidsgames/src/widgets/games/linear_progress.dart';

class LoadingPage extends StatefulWidget {
  final Widget child;
  const LoadingPage({super.key, 
    required this.child,
  });

  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  BaseGameModel? baseGameModel;
  late Responsive responsive;
  late Orientation orientation;
  double percent = 100;
  bool showTutorial = false;

  Game? game;
  @override
  void initState() {
    super.initState();
    setTime();
  }

  @override
  void dispose() {
    baseGameModel?.isPlaying = true;
    baseGameModel?.stopAudio();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    game = Provider.of<GameService>(context).game;
    checkGameTutorial();
    baseGameModel = Provider.of<BaseGameModel>(context);
    responsive = Responsive(context);
    orientation = MediaQuery.of(context).orientation;
    // this.checkGameTutorial();
    return percent > 0
        ? Scaffold(
            body: Container(
              width: double.infinity,
              height: double.infinity,
              color: const Color(0xff27839B),
              child: GameBackground(
                children: [
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: responsive.wp(
                              orientation == Orientation.landscape ? 35 : 60),
                          child: BorderedText(text: "AcamyKids"),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          width: responsive.wp(
                            orientation == Orientation.landscape ? 50 : 70,
                          ),
                          child: LinearProgress(percent: percent),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        : (showTutorial
            ? ((game != null)
                ? Container()
                : Container())
            : widget.child);
  }

  void setTime() async {
    const second = Duration(milliseconds: 400);
    Timer.periodic(second, (Timer timer) {
      if (percent == 0) {
        timer.cancel();
        if (BaseGameModel.bg.isPlaying) {
          return;
        } else {
          BaseGameModel.bg.play();
        }
      } else {
        percent -= 5;
        setState(() {});
        setTime();
      }
    });
  }

  Future<void> checkGameTutorial() async {
    
  }

  void notShowTutorial() {
    
  }

  
}
