import 'package:acamykidsgames/src/bloc/content_game_bloc.dart';
import 'package:acamykidsgames/src/models/games_model.dart';
import 'package:acamykidsgames/src/pages/games/main/data/base_game_data.dart';
import 'package:acamykidsgames/src/utils/collection.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/content/game_item.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_modal.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SectionGame extends StatefulWidget {
  const SectionGame({super.key});

  @override
  State<SectionGame> createState() => _SectionGameState();
}

class _SectionGameState extends State<SectionGame> {

  Game? currentGame;
  bool _salir = false;

  
  @override
  void initState() {
    super.initState();
    ContentGameBloc.i.gameUnits = getSubjectGamesMap(1)?.units ?? [];
  }


  @override
  Widget build(BuildContext context) {
    final Responsive responsive = Responsive(context);
    final ContentGameBloc contentGameBloc = Provider.of<ContentGameBloc>(context);

    return Scaffold(
      backgroundColor: const Color(0xff23232),
      body: Stack(
        children: [
          LayoutBuilder(
            builder: (context, constraints) {
              return ListView.builder(
                physics: BouncingScrollPhysics(),
                padding: EdgeInsets.symmetric(
                  vertical: responsive.ip(2.0),
                  horizontal: responsive.ip(2.0),
                ),
                itemCount: contentGameBloc.gameUnits.length,
                itemBuilder: (context, i) {
                  double _itemWidth = responsive.ip(24);
                  double _itemMarginH = responsive.ip(1.5); 
      
                  List<List<Game>> rowGames = toResponsiveList(
                    contentGameBloc.gameUnits[i].games.expand((x) => x).toList(),
                    _itemWidth + (_itemMarginH * 2),
                    constraints.maxWidth
                  );
      
                  return Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: responsive.ip(1)),
                        width: double.maxFinite,
                        child: Text(
                          'Unidad ${contentGameBloc.gameUnits[i].unidadNombre}',
                          style: TextStyle(
                            letterSpacing: 0.5,
                            color: Colors.white,
                            fontSize: responsive.ip(1.7),
                          ),
                        ),
                      ),
                      SizedBox(height: responsive.ip(1.5)),
                      ..._createRows(rowGames, responsive, _itemWidth, _itemMarginH),
                    ],
                  );
                },
              );
            }
          ),
          if (_salir)
          GameModal(
            game: currentGame!,
            closeModal: () => _closeModal(),
          ),
        ],
      ),
    );
  }
  
  List<Widget> _createRows(List<List<Game>> rowGames, Responsive responsive, double itemWidth, double itemMarginH) {
    return List.generate(
      rowGames.length,
      (i) => _createRow(rowGames[i], responsive, itemWidth, itemMarginH),
    );
  }

  Widget _createRow(List<Game> games, Responsive responsive, double itemWidth, double itemMarginH) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(
        games.length,
        (i) => Container(
          margin: EdgeInsets.symmetric(
            horizontal: itemMarginH,
            vertical: responsive.ip(1),
          ),
          child: GameItem(
            width: itemWidth,
            game: games[i],
            onPress: () => _openModal(games[i]),
          ),
        ),
      ),
    );
  }



  void _openModal(Game game) {
    setState(() {
      _salir = true;
      currentGame = game;
      // currentAsignaturaId = asignaturaId;
    });
  }

  void _closeModal() {
    setState(() {
      _salir = false;
      currentGame = null;
      // currentAsignaturaId = null;
    });
  }
}