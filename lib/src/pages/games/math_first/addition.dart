import 'dart:async';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';
import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/utils/random_number.dart';

// import 'package:acamykidsgames/src/services/game/data_gamev2_service.dart';
import 'package:acamykidsgames/src/services/game/games_service.dart';

import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
import 'package:acamykidsgames/src/widgets/games/main/end_game_page.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_scaffold.dart';
import 'package:acamykidsgames/src/widgets/games/basic_button/game_button.dart';
import 'package:acamykidsgames/src/widgets/games/buttons/game_buttons_with_audio.dart';

class AdditionPage extends StatefulWidget {
  @override
  _AdditionPageState createState() => _AdditionPageState();
}

class _AdditionPageState extends State<AdditionPage> {
  int? gameId;
  int? asignaturaId;
  BaseGameModel? baseGameModel;
  late Responsive responsive;
  bool firstLoad = true;
  bool buttonPressed = false;
  bool endGame = false;
  String statement = '';

  List<String> players = ["Adrián", "Luis"];
  List<int> numbers = [];
  List<int> options = [];
  List<GameButtonStatus> states = [];

  @override
  void initState() {
    super.initState();
    setNumber();
  }

  void setNumber() {
    statement = '¿Cuántos goles anotaron Adrian y Luis en total?';
    if (!firstLoad) {
      baseGameModel?.loadAudio("mate1005_0");
    }
    buttonPressed = false;
    numbers.clear();
    numbers.add(generateRandom(min: 1, max: 7));
    numbers.add(generateRandom(min: 1, max: 7));
    states = List.generate(4, (index) => GameButtonStatus.active);

    options.clear();
    options.add(numbers[0] + numbers[1]);
    while (options.length < 4) {
      int response = generateRandom(min: 1, max: 13);
      if (!options.contains(response)) {
        options.add(response);
      }
    }
    options.shuffle();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    responsive = Responsive(context);
    baseGameModel = Provider.of<BaseGameModel>(context);
    gameId = Provider.of<GameService>(context).gameId;
    asignaturaId = Provider.of<GameService>(context).asignaturaId;
    if (firstLoad) {
      firstLoad = false;
      baseGameModel?.loadAudio("mate1005_0");
    }
    return endGame
        ? EndGamePage(score: baseGameModel?.score, endGame: _endGame)
        : GameScaffold(
            score: baseGameModel?.score,
            endGame: _endGame,
            totalTime: 100,
            builder: Builder(builder: (context) => _container(context)),
            statement: statement,
          );
  }

  Widget _container(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/games/background/soccer.png',
            width: responsive.wp(100),
            height: responsive.hp(100),
            fit: BoxFit.cover,
          ),
        ),
        Column(
          children: [
            Expanded(
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    balls(0),
                    kids(),
                    balls(1),
                  ],
                ),
              ),
            ),
            GameButtonsWithAudio(
              baseGameModel: baseGameModel,
              options: options,
              states: states,
              sendAnswer: _sendAnswer,
            ),
          ],
        ),
      ],
    );
  }

  Widget kids() {
    return Container(
      child: Row(
        children: [
          Image.asset(
            'assets/games/items/kid2.png',
            height: responsive.hp(40),
          ),
          Image.asset(
            'assets/games/items/kid1.png',
            height: responsive.hp(40),
          )
        ],
      ),
    );
  }

  Widget balls(int index) {
    return Column(
      children: [
        SizedBox(height: responsive.hp(3)),
        Container(
          padding: EdgeInsets.all(responsive.ip(1)),
          child: GradientText(
            text: 'Goles de ${players[index]}',
            fontSize: 2.5,
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(25),
          ),
        ),
        SizedBox(height: responsive.hp(3)),
        Container(
          width: responsive.wp(30),
          alignment: Alignment.center,
          child: Wrap(
            spacing: 8,
            runSpacing: 4,
            children: List.generate(
              numbers[index],
              (i) => Image.asset(
                'assets/games/items/soccerball.png',
                height: responsive.hp(15),
              ),
            ),
          ),
        )
      ],
    );
  }

  void _sendAnswer(int buttonIndex) {
    if (!buttonPressed) {
      buttonPressed = true;
      if ((numbers[0] + numbers[1]) == options[buttonIndex]) {
        states[buttonIndex] = GameButtonStatus.success;
        baseGameModel?.correctAnswer();
        baseGameModel?.loadAudio("muy_bien");
        baseGameModel?.score += 10;
        statement = '¡Muy bien!';
        Timer(Duration(milliseconds: 850), () {
          setNumber();
        });
      } else {
        int correctIndex = options.indexOf(numbers[0] + numbers[1]);
        states[correctIndex] = GameButtonStatus.success;
        states[buttonIndex] = GameButtonStatus.error;
        baseGameModel?.wrongAnswer();
        baseGameModel?.loadAudio("incorrecto");
        statement = '¡Ups! Incorrecto';
        baseGameModel?.score -= 10;
        Timer(Duration(milliseconds: 1600), () => setNumber());
      }
      setState(() {});
    }
  }

  void _endGame(bool status) {
    baseGameModel?.stopAudio();
    setState(() {
      endGame = status;
      if (!endGame)
        _resetGame();
      else {
        // DataGamev2Service.sendData(gameId, asignaturaId, baseGameModel?.score);
      }
    });
  }

  void _resetGame() {
    baseGameModel?.resume();
    baseGameModel?.score = 0;
    this.setNumber();
  }
}
