import 'dart:async';
import 'dart:math';
import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
// import 'package:acamykidsgames/src/services/game/data_gamev2_service.dart';
import 'package:acamykidsgames/src/services/game/games_service.dart';
import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/widgets/games/basic_button/game_button.dart';
import 'package:acamykidsgames/src/widgets/games/buttons/game_buttons_with_audio.dart';
import 'package:acamykidsgames/src/widgets/games/main/end_game_page.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:acamykidsgames/src/utils/random_number.dart';

class NumbersOneToNinePage extends StatefulWidget {
  NumbersOneToNinePage({Key? key}) : super(key: key);

  @override
  _NumbersState createState() => _NumbersState();
}

class _NumbersState extends State<NumbersOneToNinePage> {
  int? gameId;
  int? asignaturaId;
  BaseGameModel? baseGameModel;
  late Responsive responsive;
  bool firstLoad = true;
  bool endGame = false;
  int totalTime = 100;
  late Image bg;
  int nMonk = 0;
  List<GameButtonStatus> status = [];
  List<int> alternatives = [];
  List<int> monkToShow = [];
  int correctAnswer = 0;
  bool answered = false;

  String statement = '';

  @override
  void initState() {
    super.initState();
    bg = Image.asset("assets/games/background/monkeys.png");
    _setGame();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(bg.image, context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _setGame() {
     if (!firstLoad) {
      baseGameModel?.loadAudio("mate1001");
    }
    setState(() {
      statement = "¿Cuántos monos puedes ver?";
      monkToShow.clear();
      alternatives.clear();
      answered = false;
      status = [
        GameButtonStatus.active,
        GameButtonStatus.active,
        GameButtonStatus.active,
        GameButtonStatus.active,
      ];
      nMonk = generateRandom(min: 1, max: 9);
      correctAnswer = nMonk;
      List.generate(nMonk, (index) {
        int nMon = generateRandom(min: 1, max: 10);
        while (monkToShow.contains(nMon)) {
          nMon = generateRandom(min: 1, max: 10);
        }
        monkToShow.add(nMon);
      });
      _setAlternatives();
    });
  }

  void _setAlternatives() {
    alternatives.add(nMonk);
    int count = 0;
    while (count < 3) {
      int option = generateRandom(min: 1, max: 9);
      if (!alternatives.contains(option)) {
        alternatives.add(option);
        count++;
      }
    }
    alternatives.shuffle();
  }

  Widget _monkeysContainer() {
    return Container(
      child: Stack(
        children: List.generate(nMonk, (index) {
          return _monkey(monkToShow[index]);
        }),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    responsive = Responsive(context);
    gameId = Provider.of<GameService>(context).gameId;
    asignaturaId = Provider.of<GameService>(context).asignaturaId;
    baseGameModel = Provider.of<BaseGameModel>(context);
    if (firstLoad) {
      firstLoad = false;
      baseGameModel?.loadAudio("mate1001");
    }

    return endGame
        ? EndGamePage(score: baseGameModel?.score, endGame: _endGame)
        : GameScaffold(
            score: baseGameModel?.score,
            endGame: _endGame,
            totalTime: totalTime,
            statement: statement,
            builder: Builder(
              builder: (context) => _container(context),
            ),
          );
  }

  Widget _container(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Container(
              width: responsive.width,
              height: responsive.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  colorFilter: ColorFilter.mode(
                    Colors.white.withOpacity(0.4),
                    BlendMode.lighten,
                  ),
                  image: bg.image,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          _monkeysContainer(),
          Container(
            alignment: Alignment.bottomCenter,
            child: GameButtonsWithAudio(
              baseGameModel: baseGameModel,
              options: alternatives,
              states: status,
              sendAnswer: chechAnswer,
            ),
          ),
        ],
      ),
    );
  }

  void chechAnswer(int indexButton) {
    if (!answered) {
      if (alternatives[indexButton] == correctAnswer) {
        answered = true;
        status[indexButton] = GameButtonStatus.success;
        baseGameModel?.loadAudio("muy_bien");
        baseGameModel?.correctAnswer();
        baseGameModel?.score += 5;
        statement = "¡Muy bien!";
      } else {
        int correctIndex = alternatives.indexOf(correctAnswer);
        status[correctIndex] = GameButtonStatus.success;
        status[indexButton] = GameButtonStatus.error;
        baseGameModel?.wrongAnswer();
        answered = true;
        baseGameModel?.score -= 5;
        statement =  "¡Oh no! Hay $correctAnswer monos";
      }
      setState(() {});
      Timer(Duration(milliseconds: 1500), () {
        _setGame();
      });
    }
  }

  void _resetGame() {
    baseGameModel?.resume();
    baseGameModel?.score = 0;
    _setGame();
  }

  void _endGame(bool status) {
    baseGameModel?.stopAudio();
    setState(() {
      endGame = status;
      if (!endGame)
        _resetGame();
      else {
        // DataGamev2Service.sendData(gameId, asignaturaId, baseGameModel?.score);
      }
    });
  }

  Widget _monkey(int monkey) {
    switch (monkey) {
      case 1:
        return Positioned(
          top: responsive.hp(15),
          left: responsive.wp(12),
          child: Container(
            width: responsive.wp(10),
            height: responsive.hp(30),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/games/items/monkey1.png"),
                    fit: BoxFit.fill)),
          ),
        );
      case 2:
        return Positioned(
          top: responsive.ip(4),
          right: responsive.ip(1),
          child: Container(
            width: responsive.wp(9),
            height: responsive.hp(30),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/games/items/monkey5.png"),
                    fit: BoxFit.fill)),
          ),
        );
      case 3:
        return Positioned(
          top: responsive.hp(36),
          left: responsive.wp(44),
          child: Transform(
            transform: Matrix4.rotationY(pi),
            child: Container(
              width: responsive.wp(10),
              height: responsive.hp(19),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                          "assets/games/items/monkey6.png"), //rotar mono
                      fit: BoxFit.fill)),
            ),
          ),
        );
      case 4:
        return Positioned(
          top: responsive.hp(36),
          left: responsive.wp(49),
          child: Container(
            width: responsive.wp(10),
            height: responsive.hp(19),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/games/items/monkey6.png"),
                    fit: BoxFit.fill)),
          ),
        );
      case 5:
        return Positioned(
          bottom: responsive.ip(7),
          left: responsive.ip(4),
          child: Container(
            width: responsive.wp(10),
            height: responsive.hp(20),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/games/items/monkey2.png"),
                    fit: BoxFit.fill)),
          ),
        );
      case 6:
        return Positioned(
          bottom: responsive.ip(9),
          left: responsive.ip(16),
          child: Container(
            width: responsive.wp(10),
            height: responsive.hp(25),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/games/items/monkey3.png"),
                    fit: BoxFit.fill)),
          ),
        );
      case 7:
        return Positioned(
          bottom: responsive.ip(8),
          left: responsive.ip(32),
          child: Container(
            width: responsive.wp(12),
            height: responsive.hp(15),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/games/items/monkey7.png"),
                    fit: BoxFit.fill)),
          ),
        );
      case 8:
        return Positioned(
          top: responsive.ip(5),
          right: responsive.ip(19),
          child: Container(
            width: responsive.wp(10),
            height: responsive.hp(30),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/games/items/monkey1.png"),
                    fit: BoxFit.fill)),
          ),
        );
      case 9:
        return Positioned(
          bottom: responsive.ip(6),
          right: responsive.ip(5),
          child: Transform(
            transform: Matrix4.rotationY(pi),
            child: Container(
              width: responsive.wp(12),
              height: responsive.hp(20),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/games/items/monkey4.png"),
                      fit: BoxFit.fill)),
            ),
          ),
        );
      case 10:
        return Positioned(
          bottom: responsive.ip(6),
          right: responsive.ip(5),
          child: Container(
            width: responsive.wp(12),
            height: responsive.hp(20),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image:
                        AssetImage("assets/games/items/monkey4.png"), // rotar
                    fit: BoxFit.fill)),
          ),
        );
      default:
        return Container();
    }
  }
}
