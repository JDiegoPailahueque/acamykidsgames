import 'dart:async';
import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/utils/random_number.dart';

// import 'package:acamykidsgames/src/services/game/data_gamev2_service.dart';
import 'package:acamykidsgames/src/services/game/games_service.dart';

import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
import 'package:acamykidsgames/src/widgets/games/main/end_game_page.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_scaffold.dart';
import 'package:acamykidsgames/src/widgets/games/basic_button/game_button.dart';
import 'package:acamykidsgames/src/widgets/games/buttons/game_buttons_with_audio.dart';

class TheTenPage extends StatefulWidget {
  @override
  _TheTenPageState createState() => _TheTenPageState();
}

class _TheTenPageState extends State<TheTenPage> {
  int? gameId;
  int? asignaturaId;
  BaseGameModel? baseGameModel;
  late Responsive responsive;
  bool firstLoad = true;

  bool buttonPressed = false;
  bool endGame = false;
  String statement = '';

  int number = 0;
  List<int> numbers = List.generate(9, (index) => index + 1);
  int currentStep = 0;
  List<int> order = [];
  List<int> options = [];
  List<GameButtonStatus> states = [];

  @override
  void initState() {
    super.initState();
    setNumber();
  }

  @override
  dispose() {
    super.dispose();
  }

  void setNumber() {
    statement = '¿Cuántos planetas faltan para completar una decena?';
    if (!firstLoad) {
      baseGameModel?.loadAudio("mate1003");
    }
    buttonPressed = false;
    // Cada vez que currentStep tiene resto 0 al divirse por la cantidad de números se vuele a desordenar los  números
    // Se utiliza para evitar que salgan dos números iguales
    if (currentStep % (numbers.length - 1) == 0) {
      currentStep = 0;
      numbers.shuffle();
    }
    number = numbers[currentStep];
    states = List.generate(4, (index) => GameButtonStatus.active);
    order = List.generate(16, (index) => index % 8);

    options.clear();
    options.add(10 - number);
    while (options.length < 4) {
      int response = generateRandom(min: 1, max: 10);
      if (!options.contains(response)) {
        options.add(response);
      }
    }
    options.shuffle();
  }

  @override
  Widget build(BuildContext context) {
    responsive = Responsive(context);
    baseGameModel = Provider.of<BaseGameModel>(context);
    gameId = Provider.of<GameService>(context).gameId;
    asignaturaId = Provider.of<GameService>(context).asignaturaId;
    if (firstLoad) {
      firstLoad = false;
      baseGameModel?.loadAudio("mate1003");
    }
    return endGame
        ? EndGamePage(score: baseGameModel?.score, endGame: _endGame)
        : GameScaffold(
            score: baseGameModel?.score,
            endGame: _endGame,
            totalTime: 90,
            builder: Builder(builder: (context) => _container(context)),
            statement: statement,
          );
  }

  Widget _container(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/games/background/space.png',
            width: responsive.wp(100),
            height: responsive.hp(100),
            fit: BoxFit.cover,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: responsive.wp(80),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Spacer(),
                  planets(0),
                  planets(5),
                  Spacer(),
                  GameButtonsWithAudio(
                    baseGameModel: baseGameModel,
                    options: options,
                    states: states,
                    sendAnswer: _sendAnswer,
                  ),
                ],
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Hay $number\nplanetas',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: responsive.ip(2.8),
                    color: Colors.white,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: responsive.ip(2.5)),
                  width: responsive.wp(20),
                  child: Image.asset('assets/games/items/astronaut.png'),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget planets(int initIndex) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(
        5,
        (index) => Container(
          padding: EdgeInsets.all(responsive.ip(1)),
          width: responsive.ip(10),
          child: AnimatedOpacity(
            opacity: (index + initIndex) < number ? 1 : 0,
            curve: Curves.fastOutSlowIn,
            duration: Duration(milliseconds: 400),
            child: Image.asset(
              'assets/games/items/planet${order[index + initIndex] + 1}.png',
            ),
          ),
        ),
      ),
    );
  }

  void _sendAnswer(int buttonIndex) {
    if (!buttonPressed) {
      buttonPressed = true;
      if (10 - number == options[buttonIndex]) {
        states[buttonIndex] = GameButtonStatus.success;
        baseGameModel?.correctAnswer();
        baseGameModel?.loadAudio("muy_bien");
        baseGameModel?.score += 10;
        statement = '¡Muy bien!';
        nextStep(900);
      } else {
        statement = "¡Ups! Incorrecto";
        baseGameModel?.wrongAnswer();
        baseGameModel?.loadAudio("incorrecto");
        int correctIndex = options.indexOf(10 - number);
        states[correctIndex] = GameButtonStatus.success;
        states[buttonIndex] = GameButtonStatus.error;
        baseGameModel?.score -= 10;
        nextStep(1600);
      }
    }
    setState(() {});
  }

  nextStep(milliseconds) {
    Timer(Duration(milliseconds: milliseconds), () {
      setState(() {
        currentStep++;
        setNumber();
      });
    });
  }

  void _endGame(bool status) {
    baseGameModel?.stopAudio();
    setState(() {
      endGame = status;
      if (!endGame)
        _resetGame();
      else {
        // DataGamev2Service.sendData(gameId, asignaturaId, baseGameModel?.score);
      }
    });
  }

  void _resetGame() {
    baseGameModel?.resume();
    baseGameModel?.score = 0;
    this.setNumber();
  }
}
