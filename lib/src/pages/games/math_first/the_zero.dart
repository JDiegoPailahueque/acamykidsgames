import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/utils/random_number.dart';
import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
// import 'package:acamykidsgames/src/services/game/data_gamev2_service.dart';
import 'package:acamykidsgames/src/services/game/games_service.dart';
import 'package:acamykidsgames/src/widgets/games/main/end_game_page.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_scaffold.dart';

class TheZeroPage extends StatefulWidget {
  TheZeroPage({Key? key}) : super(key: key);

  @override
  _TheZeroPageState createState() => _TheZeroPageState();
}

class _TheZeroPageState extends State<TheZeroPage> {
  int? gameId;
  int? asignaturaId;
  BaseGameModel? baseGameModel;
  late Responsive responsive;
  bool firstLoad = true;
  bool endGame = false;
  
  int totalTime = 90;
  Image? bg;
  late Random r;
  int nZeroBasket = 0;
  int correctAnswerCont = 0;
  String statement = '';

  List<int> containerList = [];

  @override
  void initState() {
    super.initState();
    this.r = new Random();
    bg = Image.asset("assets/games/background/easter.png");
    _setGame();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (bg?.image != null)
    precacheImage(bg!.image, context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _setGame() {
    statement = "Arrastra las canastas vacias hacia el número cero";
    if (!firstLoad) {
      baseGameModel?.loadAudio("mate1002");
    }
    setState(() {
      List<bool> isZeroList = [];
      correctAnswerCont = 0;
      isZeroList.clear();
      containerList.clear();
      nZeroBasket = generateRandom(min: 1, max: 6);
      int cont = 0;

      List.generate(6, (index) {
        if (cont < nZeroBasket) {
          isZeroList.add(true);
          cont++;
        } else {
          isZeroList.add(false);
        }
      });

      isZeroList.shuffle();

      List.generate(6, (index) {
        if (isZeroList[index]) {
          containerList.add(0);
        } else {
          containerList.add(generateRandom(min: 1, max: 3));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    responsive = Responsive(context);
    gameId = Provider.of<GameService>(context).gameId;
    asignaturaId = Provider.of<GameService>(context).asignaturaId;
    baseGameModel = Provider.of<BaseGameModel>(context);
    if (firstLoad) {
      firstLoad = false;
      baseGameModel?.loadAudio("mate1002");
    }

    return endGame
        ? EndGamePage(score: baseGameModel?.score, endGame: _endGame)
        : GameScaffold(
            score: baseGameModel?.score,
            endGame: _endGame,
            totalTime: totalTime,
            statement: statement,
            builder: Builder(
              builder: (context) => _container(context),
            ),
          );
  }

  Widget _container(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Container(
              width: responsive.width,
              height: responsive.height,
              decoration: (bg?.image != null)
              ? BoxDecoration(
                  image: DecorationImage(
                      colorFilter: ColorFilter.mode(
                          Colors.white.withOpacity(0.5), BlendMode.lighten),
                      image: bg!.image,
                      fit: BoxFit.fill)) : null,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: responsive.wp(10)),
            child: Center(
              child: Row(
                children: <Widget>[
                  basketsContainer(),
                  Spacer(),
                  zero(),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget basketsContainer() {
    return Container(
      width: responsive.wp(40),
      height: responsive.hp(60),
      child: Wrap(
        alignment: WrapAlignment.spaceAround,
        runAlignment: WrapAlignment.spaceAround,
        spacing: responsive.ip(1),
        children: List.generate(6, (i) {
          Widget wid = basketContainer(containerList[i]);
          return Draggable<bool>(
              onDragCompleted: () {
                if (containerList[i] == 0) {
                  setState(() {
                    containerList[i] = 4;
                  });
                }
              },
              data: containerList[i] == 0,
              feedback: wid,
              child: wid,
              childWhenDragging: hideContainer());
        }),
      ),
    );
  }

  Widget hideContainer() {
    return Container(
      width: responsive.wp(10),
      height: responsive.hp(19),
    );
  }

  Widget basketContainer(int basketType) {
    return basketType == 4
        ? hideContainer()
        : Container(
            width: responsive.wp(10),
            height: responsive.hp(19),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/games/items/basket$basketType.png"),
                fit: BoxFit.fill,
              ),
            ),
          );
  }

  Widget zero() {
    return Center(
      child: DragTarget<bool>(
        builder: (context, candidateData, rejectData) => Container(
          width: responsive.wp(20),
          height: responsive.hp(50),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/games/items/zero.png"),
                  fit: BoxFit.fill)),
        ),
        onAccept: (data) {
          checkAnswer(data);
        },
      ),
    );
  }

  void checkAnswer(bool resp) {
    if (resp) {
      baseGameModel?.correctAnswer();
      correctAnswerCont++;
    } else {
      baseGameModel?.wrongAnswer();
      baseGameModel?.score -= 2;
    }
    setState(() {});

    if (correctAnswerCont == nZeroBasket) {
      statement = "¡Muy bien!";
      baseGameModel?.loadAudio("muy_bien");
      baseGameModel?.score += 10;
      Timer(Duration(milliseconds: 800), () {
        setState(() {
          _setGame();
        });
      });
    }
  }

  void _resetGame() {
    baseGameModel?.resume();
    baseGameModel?.score = 0;
    _setGame();
  }

  void _endGame(bool status) {
    baseGameModel?.stopAudio();
    setState(() {
      endGame = status;
      if (!endGame)
        _resetGame();
      else {
        // DataGamev2Service.sendData(gameId, asignaturaId, baseGameModel?.score);
      }
    });
  }
}
