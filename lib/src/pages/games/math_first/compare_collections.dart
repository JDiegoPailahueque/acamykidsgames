import 'dart:async';
import 'package:acamykidsgames/src/widgets/games/text/gradient_text.dart';
import 'package:flutter/material.dart';

import 'package:acamykidsgames/src/utils/responsive.dart';
import 'package:acamykidsgames/src/utils/random_number.dart';

// import 'package:acamykidsgames/src/services/game/data_gamev2_service.dart';
import 'package:acamykidsgames/src/services/game/games_service.dart';

import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
import 'package:acamykidsgames/src/widgets/games/main/end_game_page.dart';
import 'package:acamykidsgames/src/widgets/games/main/game_scaffold.dart';

class CompareCollectionsPage extends StatefulWidget {
  @override
  _CompareCollectionsPageState createState() => _CompareCollectionsPageState();
}

class _CompareCollectionsPageState extends State<CompareCollectionsPage> {
  int? gameId;
  int? asignaturaId;
  BaseGameModel? baseGameModel;
  late Responsive responsive;
  bool buttonPressed = false;
  bool endGame = false;
  bool firstLoad = true;
  String statement = '';

  List<int> order = List.generate(4, (index) => index);
  List<bool> drops = List.generate(4, (index) => false);
  List<int> dropsIndex = [];
  List<String> statements = [
    'menor a mayor, de izquierda a derecha',
    'mayor a menor, de izquierda a derecha'
  ];
  int type = 0;

  @override
  void initState() {
    super.initState();
    setTowers();
  }

  @override
  dispose() {
    super.dispose();
  }

  void setTowers() {
    buttonPressed = false;
    type = generateRandom(min: 0, max: 2);
    statement = 'Ordena los bloques de ${statements[type]}';

    if (!firstLoad) {
      baseGameModel?.loadAudio("mate1004_$type");
    }

    drops = List.generate(4, (index) => false);
    order.shuffle();

    if (type == 0) {
      dropsIndex = List.generate(4, (index) => index);
    } else {
      dropsIndex = List.generate(4, (index) => 3 - index);
    }
  }

  @override
  Widget build(BuildContext context) {
    responsive = Responsive(context);
    baseGameModel = Provider.of<BaseGameModel>(context);
    gameId = Provider.of<GameService>(context).gameId;
    asignaturaId = Provider.of<GameService>(context).asignaturaId;
    if (firstLoad) {
      firstLoad = false;
      baseGameModel?.loadAudio("mate1004_$type");
    }
    return endGame
        ? EndGamePage(score: baseGameModel?.score, endGame: _endGame)
        : GameScaffold(
            score: baseGameModel?.score,
            endGame: _endGame,
            totalTime: 80,
            builder: Builder(builder: (context) => _container(context)),
            statement: statement,
          );
  }

  Widget _container(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/games/background/shelf2.png',
            width: responsive.wp(100),
            height: responsive.hp(100),
            fit: BoxFit.fill,
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            dropItems(),
            dragItems(),
          ],
        ),
      ],
    );
  }

  Widget dropItems() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: List.generate(
        4,
        (i) => DragTarget<int>(
          builder: (BuildContext context, List<int?> incoming, List rejected) {
            if (drops[i]) {
              // Elemento ya arrastrado
              return Container(
                margin: EdgeInsets.only(top: responsive.ip(2.45)),
                child: tower(dropsIndex[i] + 1),
              );
            } else {
              // Target Container
              return Padding(
                padding: EdgeInsets.only(
                  top: responsive.ip(6),
                  bottom: responsive.ip(2),
                ),
                child: Container(
                  height: responsive.hp(20),
                  width: responsive.hp(20),
                  child: Center(
                    child: GradientText(text: getText(i), fontSize: 2.2),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                ),
              );
            }
          },
          onAccept: (data) => _checkAnswer(i, data),
        ),
      ),
    );
  }

  String getText(int index) {
    if (index == 0 && type == 0 || index == 3 && type == 1) return 'Menor';
    if (index == 0 && type == 1 || index == 3 && type == 0) return 'Mayor';
    return '';
  }

  Widget dragItems() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: List.generate(
        4,
        (i) => (!drops[dropsIndex[order[i]]])
            ? Draggable(
                child: tower(order[i] + 1),
                feedback: tower(order[i] + 1),
                data: order[i],
              )
            : Container(),
      ),
    );
  }

  Widget tower(int number) {
    return Container(
      height: responsive.hp(34),
      padding: EdgeInsets.only(bottom: responsive.hp(6)),
      child: Image.asset(
        'assets/games/items/tower$number.png',
      ),
    );
  }

  void _checkAnswer(int dropIndex, int dragIndex) {
    if (!drops[dropIndex]) {
      if (dropsIndex[dropIndex] == dragIndex) {
        drops[dropIndex] = true;
        baseGameModel?.correctAnswer();
        baseGameModel?.score += 5;
      } else {
        baseGameModel?.wrongAnswer();
        baseGameModel?.score -= 5;
      }
    }
    setState(() {});
    if (!drops.contains(false)) {
      statement = '¡Muy bien!';
      baseGameModel?.loadAudio("muy_bien");
      Timer(Duration(milliseconds: 800), () {
        setState(() {
          setTowers();
        });
      });
    }
  }

  void _endGame(bool status) {
    baseGameModel?.stopAudio();
    setState(() {
      endGame = status;
      if (!endGame)
        _resetGame();
      else {
        // DataGamev2Service.sendData(gameId, asignaturaId, baseGameModel?.score);
      }
    });
  }

  void _resetGame() {
    baseGameModel?.resume();
    baseGameModel?.score = 0;
    this.setTowers();
  }
}
