
import 'package:flutter/services.dart';

class OrientationApp {
  static void setLandscapeOrientationLock() {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft,
    ]);
  }

  static void setPortraitOrientationLock() {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
    ]);
  }

  static void setDefaultOrientation() async {
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

}