List<List<T>> toPairList<T>(List<T> data) {
  List<List<T>> pairs = [];
  for (int i = 0; i < data.length; i = i + 2) {
    if (i + 1 < data.length) {
      pairs.add([data[i], data[i + 1]]);
    } else {
      pairs.add([data[i]]);
    }
  }

  return pairs;
}

extension IterableExtension<T> on Iterable<T> {
  Iterable<T> distinctBy(Object getCompareValue(T e)) {
    var result = <T>[];
    for (var element in this) {
      if (!result.any((x) => getCompareValue(x) == getCompareValue(element))) {
        result.add(element);
      }
    }

    return result;
  }
}

List<List<T>> toRowList<T>(List<T> data, int nrows) {
  List<List<T>> pairs = [];
  for (int i = 0; i < data.length; i = i + nrows) {
    int itarget = i + nrows;
    List<T> row = [];

    if (itarget > data.length) {
      itarget = data.length;
    }
    
    for (int j = i; j < itarget; j++) {
      row.add(data[j]);
    }

    pairs.add(row);
  }

  return pairs;
}

List<List<T>> toResponsiveList<T>(List<T> data, double itemWidth, double spaceContent) {
  int nrow = spaceContent ~/ (itemWidth * 1.17);

  return toRowList(data, nrow);
}
