import 'dart:math';

double degreesToRads(num deg) {
  return (deg * pi) / 180.0;
}