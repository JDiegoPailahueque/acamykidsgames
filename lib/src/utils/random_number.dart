import 'dart:math';
import 'package:collection/collection.dart';

int generateRandom({required int min, required int max}) {
  Random rnd = new Random();
  return (min + rnd.nextInt(max - min));
}

List<int> getRandomNumbers({required int min, required int max, required int quantity, List<List<int>> historicOperations = const []}) {

  bool status = false;
  List<int> numbers = [];
  int tries = 1;
  
  historicOperations.forEach((x) { 
    x.sort();
  });

  List<String> historicOperationsKeys = List.from(historicOperations.map((x) => x.map((n) => '$n').join('')));

  do {
    tries++;
    
    numbers = List.generate(quantity, (i) => generateRandom(min: min, max: max));
    List<int> _numbers = List.from(numbers); 
    _numbers.sort();

    String? oldOperation = historicOperationsKeys.firstWhereOrNull((x) => x == _numbers.join(''));

    if (oldOperation == null || tries == 5)
    status = true;
    
  } while (!status);

  return numbers;
}

double generateRandomDouble({required double min, required double max}) {
  Random rnd = new Random();
  return rnd.nextDouble() * (max - min) + min;
}

String generateCode() {
  String code = '';
  code += _getDigit(code);
  code += _getDigit(code);
  code += _getDigit(code);
  return code;
}

String _getDigit(String code) {
  int digit = generateRandom(min: 0, max: 9);
  if (!code.contains('$digit')) {
    return '$digit';
  }
  return _getDigit(code);
}

double randomDoubleWithPrecision({required double min, required double max, required int decimals}) {
  // Generamos un número random entre min y max
  double number = Random().nextDouble() * (max - min) + min;
  // Retornamos el número aproximado a la cantidad de decimales "d"
  number = toPrecision(number, decimals);

  if (removeDecimalZeroFormat(number) != number.toString()) {
    return randomDoubleWithPrecision(min: min, max: max, decimals: decimals);
  }

  return number;
}

double toPrecision(double number, int decimals) {
  return double.parse(number.toStringAsFixed(decimals));
}

String removeDecimalZeroFormat(double number) {
  // return number.toStringAsFixed(number.truncateToDouble() == number ? 0 : 1);
  return number.toString().replaceAll(RegExp(r"([.]*0)(?!.*\d)"), "");
}

bool randomBool() {
  Random rnd = new Random();
  return rnd.nextInt(2) == 0;
}
