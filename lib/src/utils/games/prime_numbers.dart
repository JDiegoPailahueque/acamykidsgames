// 3 y 15
int getPrimeNumbersBetweenTwoNumbers(int first, int second) {
  List<int> primeNumbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97];
  int index = 0;
  int cont = 0;

  while(primeNumbers[index] <= second && index < primeNumbers.length) {
    if (primeNumbers[index] >= first && primeNumbers[index] <= second) cont++;
    index++;
  }

  return cont;
}