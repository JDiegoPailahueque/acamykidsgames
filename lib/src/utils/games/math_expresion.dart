  //  Clase para transformar una lista de string en una operacion matematica
  //  La estructura es numero, simbolo, numero,... y asi sucesivamente
  //  
class MathExpresion {
  List<String> expresion = [];

  MathExpresion(List<String> problem) {
    this.expresion = problem;
  }

  //  Busca los simbolos en la lista de string.
  //  primero busca la multiplicacion y division
  //  Luego la suma y resta
  String eval() {
    if (expresion.contains('*') || expresion.contains('/')) {
      this.multiplyDivide();
    }
    if (expresion.contains('+') || expresion.contains('-')) {
      this.additionSubtraction();
    }
    return expresion.first;
  }

  //  Busca los simbolos de '*' y '/' 
  // Cuando existe una coincidencia, toma el valor anterios y posterior para realizar la respectiva operacion.
  // Setea el resultado en el valor anterios, luego elimina los dos posteriores
  //  
  void multiplyDivide() {
    while (expresion.contains('*') || expresion.contains('/')) {
      for (int i = 0; i < expresion.length; i++) {
        switch (expresion[i]) {
          case '*':
            expresion[i - 1] = (double.parse(expresion[i - 1]) *
                    double.parse(expresion[i + 1]))
                .toString();
            expresion.removeAt(i);
            expresion.removeAt(i);
            i = 0;
            break;
          case '/':
            expresion[i - 1] = (double.parse(expresion[i - 1]) /
                    double.parse(expresion[i + 1]))
                .toString();
            expresion.removeAt(i);
            expresion.removeAt(i);
            i = 0;
            break;
        }
      }
    }
  }

  //  Busca los simbolos de '+' y '-' 
  // Cuando existe una coincidencia, toma el valor anterios y posterior para realizar la respectiva operacion.
  // Setea el resultado en el valor anterios, luego elimina los dos posteriores
  void additionSubtraction() {
    while (expresion.contains('+') || expresion.contains('-')) {
      for (int i = 0; i < expresion.length; i++) {
        switch (expresion[i]) {
          case '+':
            expresion[i - 1] = (double.parse(expresion[i - 1]) +
                    double.parse(expresion[i + 1]))
                .toString();
            expresion.removeAt(i);
            expresion.removeAt(i);
            i = 0;
            break;
          case '-':
            expresion[i - 1] = (double.parse(expresion[i - 1]) -
                    double.parse(expresion[i + 1]))
                .toString();
            expresion.removeAt(i);
            expresion.removeAt(i);
            i = 0;
            break;
        }
      }
    }
  }
}
