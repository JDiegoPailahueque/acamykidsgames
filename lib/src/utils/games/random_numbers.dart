import 'dart:math';

List<int> getRandomNumbers({
  List<int> numbers = const [],
  int min = 0,
  int max = 10,
  int multiply = 1,
  int quantity = 4,
}) {
  Random rand = Random();

  while (numbers.length < quantity) {
    int number = (rand.nextInt(max) * multiply) + min;
    if (!numbers.contains(number)) {
      numbers.add(number);
    }
  }

  return numbers..shuffle();
}
