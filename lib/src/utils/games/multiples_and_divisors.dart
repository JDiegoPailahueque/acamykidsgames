int countMultiples(int multiple, int minNumber, int maxNumber) {
  int cont = 0;
  int maxMultiple = (maxNumber / multiple).floor();

  for (int i = 1; i <= maxMultiple; i++) {
    if (multiple * i > minNumber) cont++;
  }

  return cont;
}

int countDivisors(int number, int minNumber, int maxNumber) {
  int cont = 0;
  for (int divisor = minNumber; divisor <= maxNumber; divisor++) {
    if (number % divisor == 0) cont++;
  }
  return cont;
}
