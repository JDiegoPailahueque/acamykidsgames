import 'package:flutter/material.dart';

enum GameColorStatus { active, inactive, success, error, disabled }

class GameColor {
  GameColor._();
  static const Color primary = Color(0xff8E27F1);
  // static const Color primary = Color(0x8C5200FF);
  static const Color secondary = Color(0xffFFA827);
  static const Color success = Color(0xd940B97F);
  static const Color error = Color(0xd9FA8981);

  // Color get primary => Color(0x8C5200FF);
  // Color get secondary => Color(0xffFFA827);
  // Color get success => Color(0xd940B97F);
  // Color get error => Color(0xffFA8981);
}

Color getColorByStatus(GameColorStatus status) {
  if (status == GameColorStatus.success) return Color(0xd940B97F);
  if (status == GameColorStatus.error) return GameColor.error;
  if (status == GameColorStatus.active) return Colors.white;
  if (status == GameColorStatus.inactive) return Colors.white.withAlpha(120);
  // Inactive
  return Colors.white;
}

List<Color> getColorsByStatus(GameColorStatus status) {
  if (status == GameColorStatus.active) {
    return [Color(0x8C5200FF), Color(0x8CFD28C1)];
  }
  if (status == GameColorStatus.success) {
    return [Color(0xCC3B966A), Color(0xff3B966A)];
  }
  if (status == GameColorStatus.error) {
    return [Color(0x80FF2727), Color(0xffD43481)];
  }
  if (status == GameColorStatus.disabled) {
    return [Colors.transparent, Colors.transparent];
  }
  // Inactive
  return [Color(0x40000000), Color(0x40000000)];
}
