String getDivisor(int number) {
  switch (number) {
    case 2:
      return 'Mitad';
    case 3:
      return 'Tercera parte';
    case 4:
      return 'Cuarta parte';
    case 5:
      return 'Quinta parte';
    case 6:
      return 'Sexta parte';
    case 7:
      return 'Séptima parte';
    case 8:
      return 'Octava parte';
    case 9:
      return 'Novena parte';
    case 10:
      return 'Décima parte';
    case 11:
      return 'Undécima parte';
    case 12:
      return 'Duodécima parte';
    case 13:
      return 'Decimotercera parte';
    default:
      return '';
  }
}
