String getTuple(int number) {
  switch (number) {
    case 2:
      return 'Doble';
    case 3:
      return 'Triple';
    case 4:
      return 'Cuádruple';
    case 5:
      return 'Quíntuple';
    case 6:
      return 'Séxtuple';
    case 7:
      return 'Séptuple';
    case 8:
      return 'Óctuple';
    case 9:
      return 'Nónuplo';
    case 10:
      return 'Décuplo';
    case 11:
      return 'Undécuple';
    case 12:
      return 'Duodécuplo';
    case 13:
      return 'Terciodécuplo';
    default:
      return '';
  }
}
