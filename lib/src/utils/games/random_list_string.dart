import 'dart:math';

class RandomListString {
  static Random random = new Random();

  static List<RandomString> getRandomListString(List<String> list) {
    List<RandomString> randomStringList = [];
    List<String> stringList = [];

    while (randomStringList.length != list.length) {
      int rand = random.nextInt(list.length);
      if (!stringList.contains(list[rand])) {
        stringList.add(list[rand]);
        randomStringList.add(RandomString(id: randomStringList.length, value: list[rand]));
      }
    }
    return randomStringList;
  }
}

class RandomString {
  int id;
  String value;

  RandomString({
    required this.id,
    required this.value,
  });
}
