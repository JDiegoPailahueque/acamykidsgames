class NumberToLetters {
  static String getUnidades(int number) {
    switch (number) {
      case 1:
        return "UNO";
      case 2:
        return "DOS";
      case 3:
        return "TRES";
      case 4:
        return "CUATRO";
      case 5:
        return "CINCO";
      case 6:
        return "SEIS";
      case 7:
        return "SIETE";
      case 8:
        return "OCHO";
      case 9:
        return "NUEVE";
    }
    return "";
  }

  static String getMultiples(int number) {
    switch (number) {
      case 2:
        return "doble";
      case 3:
        return "triple";
      case 4:
        return "cuáduple";
      case 5:
        return "quíntuple";
      case 6:
        return "séxtuple";
      case 7:
        return "séptuple";
      case 8:
        return "óctuple";
      case 9:
        return "nónuple";
    }
    return "";
  }

  static String getDecenas(int number) {
    int decena = (number / 10).floor();
    int unidad = number - (decena * 10);

    switch (decena) {
      case 1:
        switch (unidad) {
          case 0:
            return 'DIEZ';
          case 1:
            return 'ONCE';
          case 2:
            return 'DOCE';
          case 3:
            return 'TRECE';
          case 4:
            return 'CATORCE';
          case 5:
            return 'QUINCE';
          default:
            return 'DIECI' + getUnidades(unidad);
        }
      case 2:
        switch (unidad) {
          case 0:
            return 'VEINTE';
          default:
            return 'VEINTI' + getUnidades(unidad);
        }
      case 3:
        return decenasY('TREINTA', unidad);
      case 4:
        return decenasY('CUARENTA', unidad);
      case 5:
        return decenasY('CINCUENTA', unidad);
      case 6:
        return decenasY('SESENTA', unidad);
      case 7:
        return decenasY('SETENTA', unidad);
      case 8:
        return decenasY('OCHENTA', unidad);
      case 9:
        return decenasY('NOVENTA', unidad);
    }
    return getUnidades(unidad);
  }

  static String decenasY(String strSin, int numUnidades) {
    if (numUnidades > 0) return strSin + " Y " + getUnidades(numUnidades);
    return strSin;
  }

  static String getCentenas(int number) {
    int centenas = (number / 100).floor();
    int decenas = number - (centenas * 100);

    switch (centenas) {
      case 1:
        if (decenas > 0)
          return "CIENTO " + getDecenas(decenas);
        else
          return "CIEN";
      case 2:
        return "DOSCIENTOS " + getDecenas(decenas);
      case 3:
        return "TRESCIENTOS " + getDecenas(decenas);
      case 4:
        return "CUATROCIENTOS " + getDecenas(decenas);
      case 5:
        return "QUINIENTOS " + getDecenas(decenas);
      case 6:
        return "SEISCIENTOS " + getDecenas(decenas);
      case 7:
        return "SETECIENTOS " + getDecenas(decenas);
      case 8:
        return "OCHOCIENTOS " + getDecenas(decenas);
      case 9:
        return "NOVECIENTOS " + getDecenas(decenas);
    }

    return getDecenas(decenas);
  }

  // 2584 y 1000
  // 2
  // 584
  static String getSeccion(
      int number, int divisor, String strSingular, String strPlural) {
    int cientos = (number / divisor).floor();
    int resto = number - (cientos * divisor);

    String letras = "";

    if (cientos > 1) {
      if (cientos < 10)
        letras = getUnidades(cientos);
      else if (cientos < 100)
        letras = getDecenas(number);
      else if (cientos < 1000) letras = getCentenas(number);
      letras = letras + " " + strPlural;
    } else {
      letras = strSingular;
    }
    if (resto > 0) letras += "";

    return letras;
  }

  // 2584
  // 2
  // 584
  static String getMiles(int number) {
    int divisor = 1000;
    int cientos = (number / divisor).floor();
    int resto = number - (cientos * divisor);

    String strMiles = getSeccion(number, divisor, "MIL", "MIL");
    String strCentenas = getCentenas(resto);

    if (strMiles == "") return strCentenas;

    return strMiles + " " + strCentenas;
  }

  static String getMillones(int number) {
    int divisor = 1000000;
    int cientos = (number / divisor).floor();
    int resto = number - (cientos * divisor);

    String strMillones =
        getSeccion(number, divisor, "UN MILLON DE", "MILLONES DE");
    String strMiles = getMiles(resto);

    if (strMillones == "") return strMiles;

    return strMillones + " " + strMiles;
  }

  static String getStringNumber(int number) {
    if (number < 10)
      return getUnidades(number);
    else if (number < 100)
      return getDecenas(number);
    else if (number < 1000)
      return getCentenas(number);
    else if (number < 10000)
      return getMiles(number);
    else
      return getMillones(number);
  }

  static List<String> getStringListFromNumber(int number) {
    String numberToString;
    if (number < 10)
      numberToString = getUnidades(number);
    else if (number < 100)
      numberToString = getDecenas(number);
    else if (number < 1000)
      numberToString = getCentenas(number);
    else if (number < 1000000)
      numberToString = getMiles(number);
    else
      numberToString = getMillones(number);
    return numberToString.trim().toLowerCase().split(' ').toList();
  }
}
