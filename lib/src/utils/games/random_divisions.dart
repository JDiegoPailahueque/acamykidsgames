import 'dart:math';

class RandomDivision {
  static Random random = new Random();

  static List<Division> getRandomDivisions(int quantity, int max) {
    List<int> results = [];
    List<Division> randomDivisions = [];

    while (results.length < quantity / 2) {
      int divisor = random.nextInt(max) + 1;
      int result = random.nextInt(max) + 1;
      int dividend = divisor * result;
      if (!results.contains(result)) {
        results.add(result);
        randomDivisions.add(Division(value: result, text: '$dividend:$divisor'));
        randomDivisions.add(Division(value: result, text: '$result'));
      }
    }
    return randomDivisions;
  
  }
  static List<List<Division>> getListRandomDivisions(int quantity, int max) {
    List<int> results = [];
    List<Division> divisions = [];
    List<Division> quotients = [];

    while (results.length < quantity / 2) {
      int divisor = random.nextInt(max) + 1;
      int result = random.nextInt(max) + 1;
      int dividend = divisor * result;
      if (!results.contains(result)) {
        results.add(result);
        divisions.add(Division(value: result, text: '$dividend:$divisor'));
        quotients.add(Division(value: result, text: '$result'));
      }
    }
    return [divisions..shuffle(), quotients..shuffle()];
  }
}

class Division {
  int value;
  String text;
  int state;

  Division({
    required this.value,
    required this.text,
    this.state = 0,
  });
}
