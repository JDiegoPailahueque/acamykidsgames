
class TextTransform {

  static String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  static String nameCapitalize(String? s) {
    if (s == null) return '';
    List<String> nameList= s.split(' ');
    if (nameList.length == 0) return '';

    String nameCap = ''; 

    for( var i = 0 ; i < nameList.length; i++ ) { 
      if (nameList.length - 1 != i) {
          nameCap += '${capitalize(nameList[i])} ';
          continue;
      } 
      nameCap += capitalize(nameList[i]);
    }

    return nameCap;
  }

  static String nameFirstCapitalize(String? name) {
    if (name == null) return '';
    List<String> nameList= name.split(' ');
    if (nameList.length == 0) return '';
    
    String nameCap = ''; 

    for(var i = 0 ; i < 1; i++) {
      nameCap += '${capitalize(nameList[i])}';
    }

    return nameCap;
  }

  static String audioClearText(String text) {
    return text.toLowerCase()
      .replaceAll('á','a')
      .replaceAll('é','e')
      .replaceAll('í','i')
      .replaceAll('ó','o')
      .replaceAll('ú','u')
      .replaceAll('ñ','nh')
      .replaceAll(',','')
      .replaceAll(' ','_'); 
  }

  static String addZeroIfSingle(String value) {
    if (value.length > 1) return value;
    if (value.length == 1) return '0$value';
    
    return '00';
  }
}
