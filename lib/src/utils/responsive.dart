import 'dart:math' as math;
import 'package:flutter/material.dart';

class Responsive {
  double? width;
  double? height;
  double? inch;

  Responsive(BuildContext context) {
    final size = MediaQuery.of(context).size;

    width = size.width;
    height = size.height;
    inch = math.sqrt(math.pow(width!, 2) + math.pow(height!, 2));
  }

  double get w => width ?? 0;
  double get h => height ?? 0;

  double wp(double percent) {
    return width! * percent / 100;
  }

  double hp(double percent) {
    return height! * percent / 100;
  }

  double ip(double percent) {
    return inch! * percent / 100;
  }

  static bool isMobile(BuildContext context) {
    return MediaQuery.of(context).size.width < 650 || MediaQuery.of(context).orientation == Orientation.portrait;
  }
  static bool isMobile2(BuildContext context) {
    return MediaQuery.of(context).size.width < 750;
  }

  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width < 1300 && MediaQuery.of(context).size.width >= 650;

  static bool isLargeTablet(BuildContext context) => MediaQuery.of(context).size.width >= 850;
  static bool isLargeTablet2(BuildContext context) => MediaQuery.of(context).size.width >= 1200;
}
