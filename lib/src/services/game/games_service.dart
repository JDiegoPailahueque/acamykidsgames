
import 'package:acamykidsgames/src/models/games_model.dart';
import 'package:flutter/material.dart';

class GameService with ChangeNotifier {
  static final GameService i = GameService._();

  GameService._();

  Game? _game;
  int? _asignaturaId;

  Game? get game => _game;
  int get gameId => _game?.gameId ?? 0;
  int get asignaturaId => _asignaturaId ?? 0;

  set game(Game? data) {
    _game = data;
    // notifyListeners();
  }

  set asignaturaId(int? data) {
    _asignaturaId = data;
    notifyListeners();
  }
}
