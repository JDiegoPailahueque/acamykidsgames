import 'package:acamykidsgames/src/bloc/content_game_bloc.dart';
import 'package:acamykidsgames/src/pages/games/main/provider/base_game_provider.dart';
import 'package:acamykidsgames/src/pages/games/section_game.dart';
import 'package:acamykidsgames/src/services/game/games_service.dart';
import 'package:acamykidsgames/src/utils/orientation_app.dart';
import 'package:flutter/material.dart';


void main() async {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    super.initState();
    OrientationApp.setDefaultOrientation();
  }
  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => BaseGameModel.bg),
        ChangeNotifierProvider(create: (_) => GameService.i),
        ChangeNotifierProvider(create: (_) => ContentGameBloc.i),

      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SectionGame(),
      ),
    );
  }
}
